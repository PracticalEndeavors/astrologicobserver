# Copyright (C) 2020  PracticalEndeavors.com, Alexander A. Genna
# GNU General Public License, version 2, see observer.py for license information.
import tkinter as tk
import tkinter.ttk as ttk
import defaults
import se_exec

class aboutdialog(tk.Toplevel):
	def __init__(self, received_window, title = None, context=None):
		tk.Toplevel.__init__(self, received_window)
		self.baseColor = defaults.dlgColor
		self.configure(background=self.baseColor)
		self.transient(received_window)
		if title: self.title(title)
		self.received_window = received_window
		self.initial_focus = self.body(self.received_window)
		self.grab_set()
		if not self.initial_focus: self.initial_focus = self
		self.initial_focus.focus_set()
		self.protocol("WM_DELETE_WINDOW", self.ok)
		self.geometry("+%d+%d" % (received_window.winfo_rootx()+50, received_window.winfo_rooty()+50))
		
		self.wait_window(self)
		
	def body(self, dialogWindow):
		self.resizable(False, False)	#Opens as independent window, not a "tab"
		style = ttk.Style()
		style.configure('TLabel', background=self.baseColor)
		#style.configure('TEntry', borderwidth=0)
		#style.map('TEntry', background=[('disabled', self.baseColor)], relief=[('disabled', 'ridge')])
		style.configure('TButton', background=self.baseColor)
		self.applicationDisplayName = se_exec.applicationName + "\t" + se_exec.applicationVersion
		#
		self.dialogFrame=tk.Frame(self, padx=15, pady=5, bg=self.baseColor)
		#
		self.aboutText=tk.Text(self.dialogFrame, width=100, bg=self.baseColor, highlightthickness=0, bd=0, wrap="word")
		self.aboutText.tag_configure("headingText", font=("arial", 16, "bold"), justify="left")
		self.aboutText.tag_configure("bodyText", font=("arial", 14), justify="left")
		#
		self.aboutCopyText = '''
This application is an attempt to reignite the awareness for some of what has been lost to modern light
pollution, or though the disconnection of contemporary astrological practice from direct observations
of the "wandering stars" in the night sky. The planetary motion the ancients knew was much more than
the longitudinal motion of the planets through the zodiac which is so singularly focused on today.

With direct observation challenged by conditions, or when our focus is relegated to static charts; We've
lost the ability to discern the relative contextual clues of bright vs. dim and fast vs. slow; Believing that
to perceive these conditions again provides access to richer synthesis and interpretation, this software
hopes to provide visual access to these attributes in a way that reference tables just do not do justice to.
'''
		self.aboutText.insert("end", se_exec.applicationName + "\n", "headingText")
		self.aboutText.insert("end", "Purpose:\n", "headingText")
		self.aboutText.insert("end", self.aboutCopyText, "bodyText")
		self.aboutText.insert("end", "\nContact Info:", "headingText")
		self.aboutText.insert("end", "\n\t\tinfo@PracticalEndeavors.com", "bodyText")
		
		self.aboutText.pack(fill="both", side="left", anchor='w') #expand=True,
		
		#################
		# Controls
		self.buttonFrame = tk.Frame(self, bg=self.baseColor)
		self.aboutLabel=tk.Label(self.buttonFrame, text=self.applicationDisplayName, bg=self.baseColor)
		self.okButton = ttk.Button(self.buttonFrame, text="OK", width=10, command=self.ok, state='normal')
		self.aboutLabel.pack(side="left", anchor='w', padx=15, pady=5)
		self.okButton.pack(side="right", anchor='e', padx=15, pady=5)
		
		
		self.dialogFrame.pack()
		self.buttonFrame.pack(fill="x")
		
	def ok(self, event=None):
		# put focus back to the received_window window
		self.received_window.focus_set()
		self.destroy()
