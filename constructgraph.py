# Copyright (C) 2020  PracticalEndeavors.com, Alexander A. Genna
# GNU General Public License, version 2, see observer.py for license information.
import math
import defaults
import astrological_data
import astronomical_data

class drawgraph:
	def __init__(self, dataSet, canvas, graphTypeIndex, maxScreen, ignoreInvisibility=False):
		'''	Processes a list of placement objects and draw the graphs							'''
		''' properties:																			'''
		'''																						'''
		''' methods:																			'''
		'''																						'''
		output = 0	# 0 - False | 1 - True
		debug = 0	# 0 - False | 1 - True
		#
		noTheme = 0	# 0 - False | 1 - True
		dynamicBG = defaults.dynamicBG	# 0 - False | 1 - True
		#
		self.dataSet = dataSet
		self.canvas = canvas
		self.ignoreInvisibility = ignoreInvisibility
		
		max_w, max_h = maxScreen[0], maxScreen[1]
		
		planetName = self.dataSet[0].planetName
		
		# keystroke = astrological_data.astroZodiacFont[astrological_data.zodiac.index(record[4])]
		astroFont = ("astro", 20, "bold")
		
		# Pattern for dahed lines
		self.dpInvisible = (10,10)
		
		self.x_scaleMultiplier = defaults.x_scaleMultiplier
		self.y_scaleMultiplier = defaults.y_scaleMultiplier

		#
		# Theme colors and lineweights. Set in defaults.py for now.
		invisible_dim_color,invisible_dim_weight = defaults.invisible_dim_color, defaults.invisible_dim_weight
		invisible_bright_color, invisible_bright_weight = defaults.invisible_bright_color, defaults.invisible_bright_weight
		visible_dim_color, visible_dim_weight = defaults.visible_dim_color, defaults.visible_dim_weight
		visible_bright_color, visible_bright_weight = defaults.visible_bright_color, defaults.visible_bright_weight
		invisible_slow_color, invisible_slow_weight = defaults.invisible_slow_color, defaults.invisible_slow_weight
		invisible_fast_color, invisible_fast_weight = defaults.invisible_fast_color, defaults.invisible_fast_weight
		invisible_very_fast_color, invisible_very_fast_weight = defaults.invisible_very_fast_color, defaults.invisible_very_fast_weight
		visible_slow_color, visible_slow_weight = defaults.visible_slow_color, defaults.visible_slow_weight
		visible_fast_color, visible_fast_weight = defaults.visible_fast_color, defaults.visible_fast_weight
		visible_very_fast_color, visible_very_fast_weight = defaults.visible_very_fast_color, defaults.visible_very_fast_weight
		#
		#Lists:
		currentLine = []
		self.line_no_theme = []
		# Magnitude
		self.invisible_dim = []
		self.invisible_bright = []
		self.visible_dim = []
		self.visible_bright = []
		# Speed
		self.invisible_slow = []
		self.invisible_fast = []
		self.invisible_very_fast = []
		self.visible_slow = []
		self.visible_fast = []
		self.visible_very_fast = []
		#
		self.signList=[]
		#
		# What Graphtype was called of ("DEC by SPD", "DEC by MAG", "ECLAT by SPD", "ECLAT by MAG")
		self.isDeclinationGraph = self.isEclipticLatitudeGraph = self.isMagnitudeTheme = self.isSpeedTheme = False
		if graphTypeIndex % 2 == 0:	self.isSpeedTheme = True
		else: self.isMagnitudeTheme = True
		if graphTypeIndex < 2:	self.isDeclinationGraph = True
		else: self.isEclipticLatitudeGraph = True
		#
		lastSign=None
		activeLineType=None
		pointType=None
		firstSeqNo=self.dataSet[0].sequenceID
		finalSeqNo=self.dataSet[-1].sequenceID
		
		w, h = self.canvas.winfo_width(), self.canvas.winfo_height()
		# Canvas is 1px x 1px here.
		# For the graphic to grow/scale we need to increase "h"
		h = h * self.y_scaleMultiplier
		
		extentXY = self.getAxisRange(self.dataSet)
		minimumGraphX = extentXY[0][1] * self.x_scaleMultiplier
		maximumGraphX = extentXY[0][2] * self.x_scaleMultiplier
		
		# Stretch the data out if the (outer) planets graph does not extend to fill the size of the window.
		if w / maximumGraphX >= 1:
			self.x_scaleMultiplier = self.x_scaleMultiplier * (w / maximumGraphX)
			maximumGraphX = extentXY[0][2] * self.x_scaleMultiplier
		#	print("x scale", self.x_scaleMultiplier, "newtgw", maximumGraphX)

		# Addresses a graphs that start during a retrograde period.
		# Evaluate the entire dataSet and find the the smallest (negative) value that must
		# be graphed. Offset the initial record ahead by that amount (positive)
		x_pt=math.fabs(minimumGraphX)
		#x_pt=0
		y_pt=0
		for record in self.dataSet:
			# graph which value?
			if self.isDeclinationGraph: val = record.declination 		#Plot Declination
			else: val = record.eclat									#Plot Ecliptic Latitude
			if val < 0: isDirect = False
			else: isDirect = True
			
			x_pt = x_pt + (record.speed * self.x_scaleMultiplier)
			y_pt = self.scaleYValue(y=val, canvasHeight=h)
			point = (x_pt, y_pt)
			
			# Write the planet linework to baseline list
			self.line_no_theme.append(point)
			
			# Mark reference date of SequenceID @ midpoint of dataSet
			if record.sequenceID  == (len(dataSet)/2): # 183
				self.canvas.create_line(x_pt, 50, x_pt, h -40, fill=defaults.referenceMark_color, width=2, dash=(10,10))
				self.canvas.create_oval(x_pt-5,y_pt+5,x_pt+5,y_pt-5,outline=defaults.referenceMark_color, width=2)
				self.canvas.create_text(x_pt, h -24, text=record.recordDateUTC.strftime("%b %d %Y %I:%M %p %Z"), fill=defaults.referenceMark_color, anchor='s')
				if self.isDeclinationGraph:
					currentValue = str(round(record.declination, 4))
				else:
					currentValue = str(round(record.eclat, 4))
				if self.isSpeedTheme:
					self.canvas.create_text(x_pt, 33, text=str(round(record.speed,4))+ u"\u00b0" + " per/day | Dec/Lat: " + currentValue + u"\u00b0", fill=defaults.referenceMark_color, anchor='n')
				elif self.isMagnitudeTheme:
					self.canvas.create_text(x_pt, 33, text=str(round(record.apparent_magnitude,4))+" Magnitude | Dec/Lat: " + currentValue + u"\u00b0", fill=defaults.referenceMark_color, anchor='n')
				#
				# Update background for SECT of ONLY the reference date
				if dynamicBG:
					if record.sect == astrological_data.DAY:
						self.canvas.configure(bg=defaults.dlgDayChart)
					elif record.sect == astrological_data.NIGHT:
						self.canvas.configure(bg=defaults.dlgNightChart)
			#
			# Label at sign ingress
			keystroke = astrological_data.astroZodiacFont[astrological_data.zodiac.index(record.signName)]
			if lastSign == None: #Sign of first record
				lastSign = record.signName
				self.canvas.create_oval(5,31,36,1,outline=defaults.ingressLabels_color, width=2, dash=(5,5))
				self.canvas.create_text(20, 17, text=keystroke, anchor="center", fill=defaults.ingressLabels_color, font=astroFont)
				nextSign = astrological_data.zodiac[(astrological_data.zodiac.index(record.signName)+1) % 12]
			elif record.signName == nextSign:
				self.canvas.create_line(x_pt, 16, x_pt, h -37, fill=defaults.ingressLabels_color, width=2, dash=(5,5))
				self.canvas.create_oval(x_pt,31,x_pt+30,1,outline=defaults.ingressLabels_color, width=2, dash=(5,5))
				self.canvas.create_text(x_pt+15, 17, text=keystroke, anchor="center", fill=defaults.ingressLabels_color, font=astroFont)
				nextSign = astrological_data.zodiac[(astrological_data.zodiac.index(record.signName)+1) % 12]
				self.canvas.create_text(x_pt, h -5, text=record.recordDateUTC.strftime("%b %d %Y"), anchor='s', fill=defaults.ingressLabels_color)
			#
			# Points will be of the theme Speed or Magnitude
			if self.isMagnitudeTheme:
				if not record.isVisible and not record.isBright: pointType = "i_d"
				if not record.isVisible and record.isBright: pointType = "i_b"
				if record.isVisible and not record.isBright: pointType = "v_d"
				if record.isVisible and record.isBright: pointType = "v_b"
			if self.isSpeedTheme:
				if not record.isVisible and (not record.isFast and not record.isVeryFast): pointType = "i_s"
				if not record.isVisible and record.isFast: pointType = "i_f"
				if not record.isVisible and record.isVeryFast: pointType = "i_vf"
				if record.isVisible and (not record.isFast and not record.isVeryFast): pointType = "v_s"
				if record.isVisible and record.isFast: pointType = "v_f"
				if record.isVisible and record.isVeryFast: pointType = "v_vf"
			if pointType == None: print("** Logic Error ** Unknown Point Type")
			#
			if record.sequenceID == firstSeqNo: 										#First Graph Record
				activeLineType = pointType
				currentLine.append(point)
				#print("A1: New Segment", pointType, "@ point:", record.sequenceID)
			#
			if record.sequenceID > firstSeqNo and record.sequenceID < finalSeqNo:		#Middle Records
				if pointType is activeLineType:
					currentLine.append(point)											#Continue active line type
					#print("B1: Continue Segment", activeLineType,":",pointType, len(currentLine), "@ point:", record.sequenceID)
				else:
					#End this segment, start segment of new type
					if len(currentLine) > 1:
						#End segment - Write data to the correct type.
						if activeLineType == "i_d":
							self.invisible_dim.append(tuple(currentLine))
							#print("B2: End Segment", activeLineType, "@ point:", record.sequenceID, "members:", len(self.invisible_dim))
						if activeLineType == "i_b":
							self.invisible_bright.append(tuple(currentLine))
							#print("B2: End Segment", activeLineType, "@ point:", record.sequenceID, "members:", len(self.invisible_bright))
						if activeLineType == "v_d":
							self.visible_dim.append(tuple(currentLine))
							#print("B2: End Segment", activeLineType, "@ point:", record.sequenceID, "members:", len(self.visible_dim))
						if activeLineType == "v_b":
							self.visible_bright.append(tuple(currentLine))
							#print("B2: End Segment", activeLineType, "@ point:", record.sequenceID, "members:", len(self.visible_bright))
						if activeLineType == "i_s":
							self.invisible_slow.append(tuple(currentLine))
							#print("B2: End Segment", activeLineType, "@ point:", record.sequenceID, "members:", len(self.invisible_slow))
						if activeLineType == "i_f":
							self.invisible_fast.append(tuple(currentLine))
							#print("B2: End Segment", activeLineType, "@ point:", record.sequenceID, "members:", len(self.invisible_fast))
						if activeLineType == "i_vf":
							self.invisible_very_fast.append(tuple(currentLine))
							#print("B2: End Segment", activeLineType, "@ point:", record.sequenceID, "members:", len(self.invisible_very_fast))
						if activeLineType == "v_s":
							self.visible_slow.append(tuple(currentLine))
							#print("B2: End Segment", activeLineType, "@ point:", record.sequenceID, "members:", len(self.visible_slow))
						if activeLineType == "v_f":
							self.visible_fast.append(tuple(currentLine))
							#print("B2: End Segment", activeLineType, "@ point:", record.sequenceID, "members:", len(self.visible_fast))
						if activeLineType == "v_vf":
							self.visible_very_fast.append(tuple(currentLine))
							#print("B2: End Segment", activeLineType, "@ point:", record.sequenceID, "members:", len(self.visible_very_fast))
						#
						# Start new segment from end of old line
						trailingGapPoint = currentLine[-1]
						activeLineType = pointType
						currentLine.clear()
						currentLine.append(trailingGapPoint)
						currentLine.append(point)
						#print("B3: New Segment", activeLineType,":",pointType, "@ point:", record.sequenceID)
					else:
						# Segment has only one point; Change type to current theme and build on to it.
						activeLineType = pointType
						currentLine.append(point)
						#print("B4: Gap Segment", record.sequenceID, "type", activeLineType,":",pointType)
						
			#
			if record.sequenceID == finalSeqNo:											#Last Graph Record
				#Add the last point and end segment -
				currentLine.append(point)
				#
				#Write data to the correct type.
				if activeLineType == "i_d":
					self.invisible_dim.append(tuple(currentLine))
				if activeLineType == "i_b":
					self.invisible_bright.append(tuple(currentLine))
				if activeLineType == "v_d":
					self.visible_dim.append(tuple(currentLine))
				if activeLineType == "v_b":
					self.visible_bright.append(tuple(currentLine))
				if activeLineType == "i_s":
					self.invisible_slow.append(tuple(currentLine))
				if activeLineType == "i_f":
					self.invisible_fast.append(tuple(currentLine))
				if activeLineType == "i_vf":
					self.invisible_very_fast.append(tuple(currentLine))
				if activeLineType == "v_s":
					self.visible_slow.append(tuple(currentLine))
				if activeLineType == "v_f":
					self.visible_fast.append(tuple(currentLine))
				if activeLineType == "v_vf":
					self.visible_very_fast.append(tuple(currentLine))
				#print("C1: End Segment", activeLineType, "@ point:", record.sequenceID)
			#
			# Keep track of which line was actively being drawn
			if self.isMagnitudeTheme:
				if not record.isVisible and not record.isBright: activeLineType = "i_d"
				if not record.isVisible and record.isBright: activeLineType = "i_b"
				if record.isVisible and not record.isBright: activeLineType = "v_d"
				if record.isVisible and record.isBright: activeLineType = "v_b"
			if self.isSpeedTheme:
				if not record.isVisible and (not record.isFast and not record.isVeryFast): activeLineType = "i_s"
				if not record.isVisible and record.isFast: activeLineType = "i_f"
				if not record.isVisible and record.isVeryFast: activeLineType = "i_vf"
				if record.isVisible and (not record.isFast and not record.isVeryFast): activeLineType = "v_s"
				if record.isVisible and record.isFast: activeLineType = "v_f"
				if record.isVisible and record.isVeryFast: activeLineType = "v_vf"
			#print(activeLineType," :current line", record.sequenceID)
			#
			#EDND of FOR LOOP.
		#__init__ Continues ....
		''' if we go for an option to ignore invisible - just switch calls to plotSolid... leave the '''
		''' Linework logic alone...	'''
		if noTheme:
			# No Theme - just draw the entire pointList
			self.plotSolidCurve(self.canvas, self.line_no_theme, "red", 2)
		else:
			if self.isMagnitudeTheme:
				#print("__init__ID",len(self.invisible_dim))
				for i_d_Segment in self.invisible_dim:
					#print("invisible_dim:\n",i_d_Segment)
					if self.ignoreInvisibility:
						self.plotSolidCurve(self.canvas, i_d_Segment, visible_dim_color, visible_dim_weight)
					else:
						self.plotDashCurve(self.canvas, i_d_Segment, invisible_dim_color, invisible_dim_weight, self.dpInvisible)
				
				#print("__init__IB",len(self.invisible_bright))
				for i_b_Segment in self.invisible_bright:
					#print("invisible_bright:\n",i_b_Segment)
					if self.ignoreInvisibility:
						self.plotSolidCurve(self.canvas, i_b_Segment, visible_bright_color, visible_bright_weight)
					else:
						self.plotDashCurve(self.canvas, i_b_Segment, invisible_bright_color, invisible_bright_weight, self.dpInvisible)

				#print("__init__VD",len(self.visible_dim))
				for v_d_Segment in self.visible_dim:
					#print("visible_dim:\n",v_d_Segment)
					self.plotSolidCurve(self.canvas, v_d_Segment, visible_dim_color, visible_dim_weight)

				#print("__init__VB",len(self.visible_bright))
				for v_b_Segment in self.visible_bright:
					#print("visible_bright:\n",v_b_Segment)
					self.plotSolidCurve(self.canvas, v_b_Segment, visible_bright_color, visible_bright_weight)
			
			if self.isSpeedTheme:
				#print("__init__IS",len(self.invisible_slow))
				for i_s_Segment in self.invisible_slow:
					#print("invisible_slow:\n",i_s_Segment)
					if self.ignoreInvisibility:
						self.plotSolidCurve(self.canvas, i_s_Segment, visible_slow_color, visible_slow_weight)
					else:
						self.plotDashCurve(self.canvas, i_s_Segment, invisible_slow_color, invisible_slow_weight, self.dpInvisible)
				
				#print("__init__IF",len(self.invisible_fast))
				for i_f_Segment in self.invisible_fast:
					#print("invisible_fast:\n",i_f_Segment)
					if self.ignoreInvisibility:
						self.plotSolidCurve(self.canvas, i_f_Segment, visible_fast_color, visible_fast_weight)
					else:
						self.plotDashCurve(self.canvas, i_f_Segment, invisible_fast_color, invisible_fast_weight, self.dpInvisible)
				
				#print("__init__IVF",len(self.invisible_very_fast))
				for i_vf_Segment in self.invisible_very_fast:
					#print("invisible_very_fast:\n",i_vf_Segment)
					if self.ignoreInvisibility:
						self.plotSolidCurve(self.canvas, i_vf_Segment, visible_very_fast_color, visible_very_fast_weight)
					else:
						self.plotDashCurve(self.canvas, i_vf_Segment, invisible_very_fast_color, invisible_very_fast_weight, self.dpInvisible)
				
				#print("__init__VS",len(self.visible_slow))
				for v_s_Segment in self.visible_slow:
					#print("visible_slow:\n",v_s_Segment)
					self.plotSolidCurve(self.canvas, v_s_Segment, visible_slow_color, visible_slow_weight)
				
				#print("__init__VF",len(self.visible_fast))
				for v_f_Segment in self.visible_fast:
					#print("visible_fast:\n",v_f_Segment)
					self.plotSolidCurve(self.canvas, v_f_Segment, visible_fast_color, visible_fast_weight)
				
				#print("__init__VVF",len(self.visible_very_fast))
				for v_vf_Segment in self.visible_very_fast:
					#print("visible_very_fast:\n",v_vf_Segment)
					self.plotSolidCurve(self.canvas, v_vf_Segment, visible_very_fast_color, visible_very_fast_weight)
		#
		# Draw elements after graphlines so the bounding box reads the extent of the graphic
		self.backgroundGraphics(self.canvas, graphTypeIndex)
		#self.drawNodes(self.canvas, self.line_no_theme, self.dataSet, planetName)

		# Update Scrollregion size
		bbox = self.canvas.bbox("all")
		srWidth, srHeight = bbox[2], bbox[3]
		self.canvas.configure(scrollregion = bbox)
		#print("scrollRegion", bbox)
		
	#---# END of __init__
	#
	# Limits
	def getAxisRange(self, dataSet):
		# Receives list of all placement objects as arg.
		# Sum all degree placements to find aggregate minimum
		# and maximum degrees traveled in the dataset.
		xPos = 0.0
		xList = [xPos]
		yListD = [] # DO NOT SUM; Declination and Latitude are absolute values.
		yListL = []
		for placement in dataSet:
			xPos = xPos + placement.speed
			xList.append(xPos)
			yListD.append(placement.declination)
			yListL.append(placement.eclat)
		minX = min(xList)
		maxX = max(xList)
		minDec = min(yListD)
		maxDec = max(yListD)
		minLat = min(yListL)
		maxLat = max(yListL)
		deltaDec = maxDec - minDec
		deltaLat = maxLat - minLat
		#Add up all the deg distance traveled
		totalDegTraveled = math.fabs(minX) + maxX
		return ((totalDegTraveled, minX, maxX),(deltaDec, minDec, maxDec),(deltaLat, minLat, maxLat))
	
	# Vert Pos = 0.5*H + (-DEC:LAT) / 100 * H
	def scaleYValue(self, y, canvasHeight):
		#y *= self.y_scaleMultiplier
		mid = canvasHeight / 2
		yScaled = mid + invertSign(y) / 100 * canvasHeight
		return yScaled

	def drawNodes(self, canvas, pointList, dataSet, name):
		# Application loads the graph of the SUN on initialization
		# context.py sets the nodes of the SUN as -1, step over the SUN
		if name.upper() == "SUN": return
		#
		# Revisit!! - The problem is:
		# Graph can be less than a whole or multiple zodiacal revolutions
		# nodes can fall before the graph starts, or multiple times in repetition
		# but for slow moving planets, it could take months to move off a specific degree.
		# Don't keep drawing the same node on nearly the same point.
		#
		# Locate the nodes for the reference date
		referenceMark = len(dataSet) // 2
		placement = dataSet[referenceMark]
		an = (placement.ascendingNode,"an")
		dn = (placement.descendingNode,"dn")
		ph = (placement.perihelion, "ph")
		ap = (placement.aphelion, "ap")
		# Sort the ecliptic positions to identify the index() number of the
		# corresponding x-y point from the pointList.
		lonList = []
		for p in dataSet:
			val = (p.eclon, p.sequenceID)
			lonList.append(tuple(val))
		lonList = sorted(lonList)
		#
		#nodeList = [an, dn, ph, ap] #ap is almost certainly being calculated wrong; http://www.true-node.com/eph3/
		nodeList = [an, dn]
		while nodeList:
			for lon in lonList:
				test_lon = lon[0]
				pointRecord = lon[1]
				point = pointList[pointRecord]
				#
				node = nodeList[0]
				node_lon = node[0]
				nodeType = node[1]
				if node_lon < test_lon and nodeType == "an":
					self.canvas.create_text(point[0], point[1], text=astronomical_data.astroNodeFont[0], anchor="s", fill="black", font=("astro", 24, "bold"))
					nodeList.pop(nodeList.index(an))
					break
				if node_lon < test_lon and nodeType == "dn":
					self.canvas.create_text(point[0], point[1], text=astronomical_data.astroNodeFont[1], anchor="s", fill="black", font=("astro", 24, "bold"))
					nodeList.pop(nodeList.index(dn))
					break
				if node_lon < test_lon and nodeType == "ph":
					self.canvas.create_text(point[0], point[1], text="P", anchor="s", fill="black", font=("arial", 24, "bold"))
					nodeList.pop(nodeList.index(ph))
					break
				if node_lon < test_lon and nodeType == "ap":
					self.canvas.create_text(point[0], point[1], text="A", anchor="s", fill="black", font=("arial", 24, "bold"))
					nodeList.pop(nodeList.index(ap))
					break
			if len(nodeList) > 0: node = nodeList[0]
	
	def backgroundGraphics(self, canvas, graphType):
		dpTropics = (20,20)
		#
		bbox = self.canvas.bbox("all") # bounding box returns tuple (WEST, NORTH, EAST, SOUTH)
		# Terms so we don't get confused when drawing declination vs ecliptic latitude
		x_left, x_right = bbox[0], bbox[2]
		y_top, y_bottom = bbox[1], bbox[3]
		y_ecliptic_equator = y_bottom / 2
		#
		# Draw the celestial equator or ecliptic centerline
		if graphType < 2:
			self.canvas.create_text(x_left+5,y_ecliptic_equator-10, text="celestial equator", fill=defaults.celestialEquator_color, anchor='w')
			self.canvas.create_line(x_left,y_ecliptic_equator, x_right,y_ecliptic_equator, width=1, fill=defaults.celestialEquator_color)
		else:
			self.canvas.create_text(x_left+5,y_ecliptic_equator-10, text="ecliptic", fill=defaults.ecliptic_color, anchor='w')
			self.canvas.create_line(x_left,y_ecliptic_equator, x_right,y_ecliptic_equator, width=1, fill=defaults.ecliptic_color)
		#
		# Draw tropic of cancer/capricorn only for Declination graphs.
		if graphType < 2:
			y_trCAN = self.scaleYValue(y=astronomical_data.tropic_of_cancer, canvasHeight=y_bottom)
			y_trCAP = self.scaleYValue(y=astronomical_data.tropic_of_capricorn, canvasHeight=y_bottom)
			
			self.canvas.create_line(x_left,y_trCAN, x_right,y_trCAN, width=2, fill=defaults.tropicsLatitudes_color, dash=dpTropics)
			self.canvas.create_line(x_left,y_trCAP, x_right,y_trCAP, width=2, fill=defaults.tropicsLatitudes_color, dash=dpTropics)
			self.canvas.create_text(x_left+5,y_trCAN-10, text="tropic of cancer", fill=defaults.tropicsLatitudes_color, anchor='w')
			self.canvas.create_text(x_left+5,y_trCAP+10, text="tropic of capricorn", fill=defaults.tropicsLatitudes_color, anchor='w')
		#
		# Intermediate line -- maybe this is too much noise. Revisit later!
		#for y in (25,20,15,10,5,-5,-10,-15,-20,-25):
		#	intermediateY = self.scaleYValue(y=y, canvasHeight=y_bottom)
		#	self.canvas.create_line(x_left,intermediateY, x_right,intermediateY, width=1, fill=defaults.intermediateLatitudes_color, dash=(5,5))

	def plotDashCurve(self, canvas, pointList, lineColor, lineWidth, linePattern):
		#print("plotDashCurve:\n",pointList)
		self.canvas.create_line(pointList, fill=lineColor, width=lineWidth, dash=linePattern, smooth=True)

	def plotSolidCurve(self, canvas, pointList, lineColor, lineWidth):
		#print("plotDashCurve:\n",pointList)
		self.canvas.create_line(pointList, fill=lineColor, width=lineWidth, smooth=True)


def invertSign(x):
	if x < 0: x = math.fabs(x)
	else: x = -x
	return x
