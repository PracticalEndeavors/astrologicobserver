# Copyright (C) 2020  PracticalEndeavors.com, Alexander A. Genna
# GNU General Public License, version 2, see observer.py for license information.
import se_exec
import math
import defaults
import astrological_data
import astronomical_data
from datetime import timedelta, datetime, tzinfo, timezone

class placementData:
	def __init__(self, planetIndex, tjd_ut, dgeo, sequenceID=None):
		'''	Details of a body for a given date																								'''
		'''	calculates:																														'''
		'''	sequenceID, planetName, eclon, 	eclat, speed, declination, elongation, apparent_magnitude, visionType, magLimit, recordDateUTC	'''
		'''	degPlacementSUN, sect, asc, dsc, mc, ascendingNode, descendingNode, perihelion, aphelion										'''
		''' evaluates:																														'''
		'''	signName, isFast, isVeryFast, isDirect, isBright, isVisible, isUnderBeams, isCombust, isCazimi, isOccidental, isOriental		'''

		output = 0	# 0 - False | 1 - True
		debug = 0	# 0 - False | 1 - True
		#
		self.sequenceID = sequenceID
		# Set the necessary flags
		iFlag = se_exec.swe.FLG_SPEED + se_exec.swe.FLG_SWIEPH + se_exec.swe.FLG_JPLEPH
		iHelFlag = se_exec.swe.FLG_SWIEPH + se_exec.swe.HELFLAG_VISLIM_DARK #Sun at Nadir makes more understandable graphs
		Normal_Snellen_Ratio = 1
		Observer_Age = 36
		Observer_Snellen_Ratio = Normal_Snellen_Ratio
		datm = [1013.25, 15, 40, 0] # Use SWEPHE system defaults (Pressure 1013.25, temperature 15, relative humidity 40.)
		dobs = [Observer_Age, Observer_Snellen_Ratio, 1, 0, 0, 0]
		#
		# GREG Date of record
		gregDate = se_exec.swe.jdut1_to_utc(tjd_ut, se_exec.swe.GREG_CAL)
		self.recordDateUTC = datetime(gregDate[0], gregDate[1], gregDate[2], gregDate[3], gregDate[4], 0).replace(tzinfo=timezone.utc)
		#
		# Calculate point
		self.planetName = astronomical_data.classicBodies[planetIndex].lower()
		data, rtn = se_exec.swe.calc_ut(tjd_ut, planetIndex, iFlag)
		# Advise if the SWIEPHE is not used; Could indicate PATH error in se_exec.py if we see "MOSHIER"
		if rtn == 260: print("*WARNING* MOSHIER ephemeris files used. Expect SLOW Computations.") # Moshier ephemeris would return +4 (260), SWE +2 (258), & JPL +1 (257)
		elif rtn == 257: print("*INFO* JPL ephemeris files used.")
		#
		self.eclon = data[0]
		self.eclat = data[1]
		self.speed = data[3]
		#
		adlFlags = se_exec.swe.FLG_EQUATORIAL
		#adlFlags = se_exec.swe.FLG_EQUATORIAL + se_exec.swe.FLG_XYZ
		data, rtn = se_exec.swe.calc_ut(tjd_ut, planetIndex, iFlag + adlFlags)
		self.declination = data[1]
		
		#
		data = se_exec.swe.pheno_ut(tjd_ut, planetIndex, iFlag)
		self.elongation = data[2]
		self.apparent_magnitude = data[4]
		
		# To determine the limiting visual magnitude in dark skies. Get the visual magnitude of an object for a given date
		# (e. g. from a call of function swe_pheno_ut(), and if mag is smaller than the value returned by swe_vis_limit_mag(),
		# then it is visible. *** No values for SUN! ***
		#
		self.isVisible = True							#<---!!! SUN can't always be visible! What about at NIGHT!
		#self.visionType, self.magLimit = None, None
		if planetIndex > astronomical_data.SUN: #Avoid swisseph.Error: it makes no sense to call swe_vis_limit_mag() for the Sun
			data = se_exec.swe.vis_limit_mag(tjd_ut, dgeo, datm, dobs, self.planetName, iHelFlag)
			'''RTN:	 -1, onerror, -2, object is below horizon, 0, OK: photopic vision, &1, OK: scotopic vision, &2, OK: near limit photopic/scotopic vision		'''
			# Keep under conditional
			self.visionType = data[0]
			self.magLimit = data[1]
			# Scotopic vision uses only rods to see, meaning that objects are visible, but appear in black and white,
			# whereas photopic vision uses cones and provides color. Photopic vision is the vision of the eye under well-lit
			# conditions. Allows significantly higher visual acuity and temporal resolution than available with scotopic vision
			# Invisibility = [1] below scotopic threshold, [2] beneath the horizon;
			if self.visionType != -2:
				if self.apparent_magnitude < self.magLimit: self.isVisible = True
				else: self.isVisible = False				#[1] below scotopic threshold
			else: self.isVisible = False					#[2] beneath the horizon
		#
		# Evaluate returned data:
		self.signName = astrological_data.zodiac[int(self.eclon // 30)]
		# Get speed data
		fastMotion = astronomical_data.fast_motion[planetIndex][1]
		avgMotion = astronomical_data.daily_motion[planetIndex][1]
		#
		absSpeed = math.fabs(self.speed)					#Retrograde speed is negative, but can be fast.
		#
		if absSpeed > fastMotion: self.isVeryFast = True
		else: self.isVeryFast = False
		if absSpeed > avgMotion: self.isFast = True
		else: self.isFast = False
		#
		# Iterate over set of these placements to find SD - SR event. We don't need more granularity than this.
		# Current motion is direct or retrograde?
		if self.speed > 0: self.isDirect = True
		else: self.isDirect = False
		#
		# Get magnitude data
		# We're calculating this since the source Max/Min values may require tuning.
		minMaxMagnitude = astronomical_data.apparent_magnitude[planetIndex]
		avgMagnitude = sum(minMaxMagnitude) / len(minMaxMagnitude)
		#
		# the more negative the value, the brighter the body
		if self.apparent_magnitude < avgMagnitude: self.isBright = True
		else: self.isBright = False
		#
		# ** warn ** if value is out of min/max range; source values may need adjustment
		if debug:
			if self.apparent_magnitude < minMaxMagnitude[0] or self.apparent_magnitude > minMaxMagnitude[1]:
				print("*WARNING* Apparent magnitude out of range. See astronomical_data.py")
		#
		# Calculate placement of the SUN
		sunData, rtn = se_exec.swe.calc_ut(tjd_ut, se_exec.swe.SUN, iFlag)
		self.degPlacementSUN = sunData[0]
		#
		# Getting SECT will require getting the degree of the ASC/DSC axis
		# and using the ecliptic LON of the SUN (to see if its above or below the ASC/DSC)
		# Configure swe_house_name args
		# swe_houses(tjd_ut, lat, lon, hsys) --- LON/LAT order in DGEO is reversed here.
		hsys = bytes('O','utf-8')	# Porphyry, really only getting the ASC & MC so any system works
		houseData = se_exec.swe.houses(tjd_ut, dgeo[1], dgeo[0], hsys)
		
		self.asc = houseData[1][0]
		self.dsc = (self.asc + 180) % 360
		self.mc = houseData[1][1]
		
		if debug:
			if self.sequenceID == 183:
				print(gregDate)
				print("asc:", houseData[1][0], "mc:", houseData[1][1], "pl:", self.eclon,"sun:", self.degPlacementSUN)
		
		# Node Placements
		#iFlag = se_exec.swe.FLG_SPEED + se_exec.swe.FLG_SWIEPH + se_exec.swe.FLG_HELCTR
		#SE_NODBIT_MEAN		1
		#SE_NODBIT_OSCU		2
		#SE_NODBIT_OSCU_BAR	4
		iMethod = se_exec.swe.NODBIT_OSCU
		#
		self.ascendingNode = -1
		self.descendingNode = -1
		self.perihelion = -1
		self.aphelion = -1
		if self.planetName.upper() != "SUN": # We don't want Nodes of the SUN
			#
			# https://groups.io/g/swisseph/topic/34505852#7651 (3.4.  Position and Speed (double xx[6])) Understanding swe_nod_aps() & swe_nod_aps_ut() return values.
			#swe_nod_aps_ut(tjd_ut,ipl,iflag,method)
			nodes = se_exec.swe.nod_aps_ut(tjd_ut, planetIndex, iFlag, iMethod) # - geocentric osculating
			#
			self.ascendingNode = nodes[0][0]	#array of 6 doubles for ascending node as numeric vector
			self.descendingNode = nodes[1][0]
			self.perihelion = nodes[2][0]
			self.aphelion = nodes[3][0]
			#
			# These values may be calculating incorrectly?
			# Test Results with this version of the python SWE lib vary from older version results
			# posted to forum above.
			#if self.sequenceID == 183:
			#	print("/n")
			#	print(self.planetName.upper())
			#	print("ascending", self.ascendingNode)
			#	print("descending", self.descendingNode)
			#	print("perihelion", self.perihelion)
			#	print("aphelion", self.aphelion)
		
		#
		# Configure SECT
		if 0 <= self.asc and 180 >= self.asc: # Then 0 ARIES (0|360 boundary) is in the TOP of the chart
			# Test if SUN is in the contiguous range of degrees in the bottom hemisphere
			if self.asc <= self.degPlacementSUN and self.degPlacementSUN <= self.dsc:
				self.sect = astrological_data.NIGHT
			else:
				self.sect = astrological_data.DAY
		else: # Then 0 ARIES (0|360 boundary) is in the BOTTOM of the chart
			# Test if SUN is in the contiguous range of degrees in the top hemisphere
			if self.asc >= self.degPlacementSUN and self.degPlacementSUN >= self.dsc:
				self.sect = astrological_data.DAY
			else:
				self.sect = astrological_data.NIGHT
		self.sect = int(self.sect)
		#
		# Relationships to the SUN
		self.ubsDeg = defaults.ubsDeg
		self.combustDeg = defaults.combustDeg
		self.cazimiDeg = defaults.cazimiDeg
		
		if self.pointInsideLimits(self.degPlacementSUN, self.ubsDeg, self.eclon):
			self.isUnderBeams = True
			if self.pointInsideLimits(self.degPlacementSUN, self.combustDeg, self.eclon):
				self.isCombust = True
				if self.pointInsideLimits(self.degPlacementSUN, self.cazimiDeg, self.eclon):
					self.isCazimi = True
				else:
					self.isCazimi = False
			else:
				self.isCombust = False
				self.isCazimi = False
		else:
			self.isUnderBeams = False
			self.isCombust = False
			self.isCazimi = False
		#
		# Position Occidental (western/setting), Oriental (eastern/rising) from the SUN
		# 'oriental of the Sun' means it precedes the Sun in diurnal motion,
		# and appears in the east in the morning, rising before the Sun.
		# 'occidental of the Sun' means that it follows the sun by diurnal motion,
		# and comes into view after sunset.
		opposition = (self.degPlacementSUN + 180) % 360
		if self.degPlacementSUN < 180:
			if self.degPlacementSUN < self.eclon < opposition:
				self.isOriental = False
				self.isOccidental = True
			else:
				self.isOriental = True
				self.isOccidental = False
		else: #
			if self.degPlacementSUN > self.eclon > opposition:
				self.isOriental = True
				self.isOccidental = False
			else:
				self.isOriental = False
				self.isOccidental = True
		
		if output:
			print(self.planetName.upper(), tjd_ut, dgeo)
			print(tjd_ut,"[",gregDate,"]")
			print("Sign:", self.signName)
			print("Direct:", self.isDirect, "Fast:", self.isFast, "VeryFast:", self.isVeryFast)
			print("Bright:", self.isBright)
			if planetIndex > astronomical_data.SUN:
				print("Visible:", self.isVisible)
				print("Type:",self.visionType,"Mag:",self.apparent_magnitude,"Limit:",self.magLimit)
			print("Lon:",self.eclon,"Lat:",self.eclat,"Dec:",self.declination,"Spd:",self.speed,"Elong:",self.elongation)
			print("Sun:", self.degPlacementSUN,"Sect:", self.sect)
			print("ASC:",self.asc,"DSC:",self.dsc,"MC:",self.mc)
	#
	#
	def pointInsideLimits(self, center, dist, pt):
		dist = math.fabs(dist)
		if (center + dist) > 360 or (center - dist) < 0:
			if (center + dist) > 360:
				lowVal = (center + dist) % 360
				highVal = center - dist
				if lowVal >= pt >= 0 or 360 >= pt >= highVal:
					return True
				else:
					return False
			elif (center - dist) < 0:
				highVal = 360 - math.fabs(center - dist)
				lowVal = (center + dist)
				if highVal <= pt < 360 or 0 < pt <= lowVal:
					return True
				else:
					return False
		else:
			highVal = center + dist
			lowVal = center - dist
			if highVal >= pt >= lowVal:
				return True
			else:
				return False
