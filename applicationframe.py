# Copyright (C) 2020  PracticalEndeavors.com, Alexander A. Genna
# GNU General Public License, version 2, see observer.py for license information.
import tkinter as tk
import tkinter.ttk as ttk
from tkinter import messagebox
import tkinter.font
import os
import math
import defaults
import se_exec
import astrological_data
import astronomical_data
import context
import placement
import constructdiagram
import constructgraph
import sign
import functions
import fileoperations
import getcontextdata
import about
from datetime import timedelta, datetime, tzinfo, timezone
#from dateutil.relativedelta import relativedelta
#from tk import Button, Checkbutton, Entry, Frame, Label, LabelFrame, Menubutton, PanedWindow, Radiobutton, Scale, Scrollbar
#from ttk import Button, Checkbutton, Entry, Frame, Label, LabelFrame, Menubutton, PanedWindow, Radiobutton, Scale, Scrollbar, Combobox, Notebook, Progressbar, Separator, Sizegrip, Treeview
debug = 0	# 0 - False | 1 - True

class observer(tk.Frame):
	def __init__(self,received_window):
		self.baseColor=defaults.dlgColor
		self.appWindow = received_window
		style=ttk.Style()
		style.configure('TLabel', background=self.baseColor)
		style.configure('TButton', background=self.baseColor)
		style.configure('TCombobox', selectbackground="none")
		style.map('TCombobox', selectbackground=[('background', "none")]) # stops the after focus grey highlight, not sure why.
		tk.Frame.__init__(self,master=self.appWindow, bg=self.baseColor)
		
		self.testForFont()
		
		self.startUI()
		self.initialValues() #Need planet data before we can draw anything!
		self.loadMenubar(self.appWindow)
		self.filesys = fileoperations.filesys()
		self.getLoadableFile()
	
	def startUI(self):
		self.maxScreen = (self.winfo_screenwidth(), self.winfo_screenheight())
		astroFont = ("astro", 18, "bold")
		
		# Controls in Top; Canvas in Bottom
		self.topFrame=tk.Frame(self, bg=self.baseColor)#, highlightbackground="green", highlightcolor="green", highlightthickness=4)
		self.topFrame.pack(fill="both")
		self.bottomFrame=tk.Frame(self, bg=self.baseColor)#, highlightbackground="red", highlightcolor="red", highlightthickness=4)
		self.bottomFrame.pack(expand=True, fill="both")
		#
		# Inner-Top Control containers
		self.leftFrame=tk.Frame(self.topFrame, padx=15, pady=5, bg=self.baseColor)#, highlightbackground="green", highlightcolor="green", highlightthickness=4)
		self.leftFrame.pack(fill="y", side='left', anchor='w')
		
		self.rightFrame=tk.Frame(self.topFrame, padx=15, pady=5, bg=self.baseColor)#, highlightbackground="green", highlightcolor="green", highlightthickness=4)
		self.rightFrame.pack(fill="y", side='right', anchor='e')
		
		self.centerFrame=tk.Frame(self.topFrame, pady=5, bg=self.baseColor)#, highlightbackground="red", highlightcolor="red", highlightthickness=4)
		self.centerFrame.pack(expand=True, fill="both", side='left', anchor='w')
		#
		# Application Version Info
		self.applicationDisplayName = se_exec.applicationName + "\t" + se_exec.applicationVersion + "  \u00A9 2020"
		self.applicationLabel=tk.Label(self, text=self.applicationDisplayName, bg=self.baseColor)
		self.applicationLabel.pack(fill="x", anchor='e', side="right")
		#
		###################################################
		#
		self.frameLeftRow1=tk.Frame(self.leftFrame, bg=self.baseColor)
		# Create labels reporting current context
		# Date/Time
		self.contextDate=tk.StringVar()
		self.contextDate.set("")
		self.dateLabel=tk.Label(self.frameLeftRow1, textvariable=self.contextDate, bg=self.baseColor)
		self.dateLabel.pack(anchor='w', side="left")
		# Lat
		self.contextLat=tk.StringVar()
		self.contextLat.set("")
		self.latLabel=tk.Label(self.frameLeftRow1, textvariable=self.contextLat, bg=self.baseColor)
		self.latLabel.pack(anchor='w', side="left", padx=5)
		# Lon
		self.contextLon=tk.StringVar()
		self.contextLon.set("")
		self.lonLabel=tk.Label(self.frameLeftRow1, textvariable=self.contextLon, bg=self.baseColor)
		self.lonLabel.pack(anchor='w', side="left")
		#
		#
		self.frameRightRow1=tk.Frame(self.rightFrame, bg=self.baseColor)
		#
		# Change the context
		self.dataButton=ttk.Button(self.frameRightRow1, text="MODIFY", width=14, command=self.getData)
		self.dataButton.pack(anchor='e', side="right")
		#
		# Create radio options for planet selection
		self.frameLeftRow2=tk.Frame(self.leftFrame, bg=self.baseColor)
		options=astronomical_data.classicBodies
		self.selection=tk.IntVar()
		for val, option in enumerate(options):
			tk.Radiobutton(self.frameLeftRow2, text=option, value=val, variable=self.selection, command=self.changePlanet, bg=self.baseColor, padx=5, pady=5).pack(side="left", anchor='w')
		#
		# Create pull-down for graph type selection
		self.graphType=tk.StringVar()
		self.themeInvisible = tk.BooleanVar()
		self.themeInvisible.set(False)
		self.frameRightRow2=tk.Frame(self.rightFrame, bg=self.baseColor)
		self.comboGT=ttk.Combobox(self.frameRightRow2, textvariable=self.graphType, width=15, state="readonly",justify="center")
		self.comboGT["values"]=("DEC by SPD", "DEC by MAG", "ECLAT by SPD", "ECLAT by MAG")
		self.comboGT.current(0)
		self.comboGT.pack(side="right", anchor='e', pady=5)
		self.comboGT.bind('<<ComboboxSelected>>', self.setGraphType)
		#
		# Right side controls toward the inside of frame appear after edge controls
		# Pack() inserts from right to left.
		self.themeInvisibleChkBtn = ttk.Checkbutton(self.frameRightRow2, text="Ignore Invisibility", variable=self.themeInvisible, onvalue=True, command=self.showInvisible)
		self.themeInvisibleChkBtn.pack(anchor='e', side="right")
		#
		# Sect
		self.frameLeftRow3=tk.Frame(self.leftFrame, bg=self.baseColor)
		self.chartSect=tk.StringVar()
		self.chartSect.set("")
		self.chartLabel=tk.Label(self.frameLeftRow3, text="Sect:", bg=self.baseColor)
		self.chartSectLabel=tk.Label(self.frameLeftRow3, textvariable=self.chartSect, bg=self.baseColor)
		self.chartLabel.pack(side="left", anchor='w', padx=0)
		self.chartSectLabel.pack(side="left", anchor='w', padx=0)
		#
		###################################################
		#
		self.frameLeftRow4=tk.Frame(self.leftFrame, bg=self.baseColor)
		#
		# Ruler,
		self.dispositorLabel=tk.Label(self.frameLeftRow4, text="Dispositor of", bg=self.baseColor)
		self.dispositorLabel.pack(side="left", anchor='w')
		#
		self.signRuler=tk.StringVar()
		self.signRuler.set("")
		self.rulerLabel=tk.Label(self.frameLeftRow4, text="Domicile:", bg=self.baseColor)
		self.signRulerLabel=tk.Label(self.frameLeftRow4, textvariable=self.signRuler, bg=self.baseColor, font=astroFont)
		self.rulerLabel.pack(side="left", anchor='w')
		self.signRulerLabel.pack(side="left", anchor='w', padx=5)
		# Triplicity lords
		self.triplicityLords=tk.StringVar()
		self.triplicityLords.set("")
		self.triplicityLabel=tk.Label(self.frameLeftRow4, text="Triplicity:", bg=self.baseColor)
		self.triplicityLordsLabel=tk.Label(self.frameLeftRow4, textvariable=self.triplicityLords, bg=self.baseColor, font=astroFont)
		self.triplicityLabel.pack(side="left", anchor='w', padx=2)
		self.triplicityLordsLabel.pack(side="left", anchor='w', padx=5)
		# Bound lords
		self.boundsLord=tk.StringVar()
		self.boundsLord.set("")
		self.boundsLabel=tk.Label(self.frameLeftRow4, text="Bound:", bg=self.baseColor)
		self.boundsLordLabel=tk.Label(self.frameLeftRow4, textvariable=self.boundsLord, bg=self.baseColor, font=astroFont)
		self.boundsLabel.pack(side="left", anchor='w', padx=2)
		self.boundsLordLabel.pack(side="left", anchor='w', padx=5)
		# Decan lord
		self.decanLord=tk.StringVar()
		self.decanLord.set("")
		self.decanLabel=tk.Label(self.frameLeftRow4, text="Decan:", bg=self.baseColor)
		self.decanLordLabel=tk.Label(self.frameLeftRow4, textvariable=self.decanLord, bg=self.baseColor, font=astroFont)
		self.decanLabel.pack(side="left", anchor='w', padx=2)
		self.decanLordLabel.pack(side="left", anchor='w', padx=5)
		#
		###################################################
		#
		self.frameRightRow3=tk.Frame(self.rightFrame, bg=self.baseColor)
		# Ecliptic Longitude Placement
		self.signPlacementText=tk.Text(self.frameRightRow3, width=20, height=2, bg=self.baseColor, highlightthickness=0, bd=0, wrap="none")
		self.signPlacementText.tag_configure("regular", font=(18), justify="right")
		self.signPlacementText.tag_configure("astro", font=("astro", 18, "bold"), justify="right")
		self.signPlacementText.tag_configure("retro", font=("astro", 18, "bold"), justify="right") #Why is the Rx so small!
		#
		self.solarRelationship=tk.StringVar()
		self.solarRelationship.set("")
		self.solarRelationshipLabel=tk.Label(self.frameRightRow3, textvariable=self.solarRelationship, bg=self.baseColor)
		#
		self.solarOrientation=tk.StringVar()
		self.solarOrientation.set("")
		self.solarOrientationLabel=tk.Label(self.frameRightRow3, textvariable=self.solarOrientation, bg=self.baseColor)
		#
		self.solarRelationshipLabel.pack(side="right", anchor='e')
		self.solarOrientationLabel.pack(side="right", anchor='e')
		self.signPlacementText.pack(side="right", anchor='e')
		#
		###################################################
		#
		self.frameLeftRow5=tk.Frame(self.leftFrame, bg=self.baseColor)
		#
		#Essential Dignity
		self.dignity=tk.StringVar()
		self.dignity.set("")
		self.dignityLabel=tk.Label(self.frameLeftRow5, text="Essential Dignity:", bg=self.baseColor)
		self.dignityTypeLabel=tk.Label(self.frameLeftRow5, textvariable=self.dignity, bg=self.baseColor)
		self.dignityLabel.pack(side="left", anchor='w')
		self.dignityTypeLabel.pack(side="left", anchor='w', padx=5)
		#
		###################################################
		#
		# Ecliptic Longitude SUN
		self.frameRightRow4=tk.Frame(self.rightFrame, bg=self.baseColor)
		#
		self.sunPlacementText=tk.Text(self.frameRightRow4, width=20, height=2, bg=self.baseColor, highlightthickness=0, bd=0, wrap="none")
		self.sunPlacementText.tag_configure("regular", font=(18), justify="right")
		self.sunPlacementText.tag_configure("astro", font=("astro", 18, "bold"), justify="right")
		#
		self.sunPlacementText.pack(side="right", anchor='e')
		#
		###################################################
		# Horizon Graphics
		self.frameCenterRow1=tk.Frame(self.centerFrame, bg=self.baseColor)
		self.horizonCanvas=tk.Canvas(self.frameCenterRow1, bd=2, relief="sunken", highlightthickness=0, bg=self.baseColor, height=150, width=150)
		self.horizonCanvas.pack(anchor='n')
		
		#
		###################################################
		# Change Context Datetime Controls
		self.frameLeftRow6 =tk.Frame(self.leftFrame, bg=self.baseColor)
		self.subtractButton=ttk.Button(self.frameLeftRow6, text="<", width=4, command=self.subtractTime)
		self.subtractButton.pack(anchor='s', side="left")
		#
		# Reset Context - NOW Button
		self.nowButton=ttk.Button(self.frameLeftRow6, text="NOW", width=15, command=self.initialValues)
		self.nowButton.pack(anchor='se', side="right", pady=5)
		#
		###################################################
		#
		self.frameRightRow5 =tk.Frame(self.rightFrame, bg=self.baseColor)
		self.addButton=ttk.Button(self.frameRightRow5, text=">", width=4, command=self.addTime)
		self.addButton.pack(anchor='s', side="right")
		#
		###################################################
		#
		# This whole frame construction / widget placement was just keep the horizonCanvas centered, and
		# not have the applications controls get too tall. Before editing turn on the commented out highlightcolor
		# color code at the end of the self.rightFrame and self.centerFrame instantiation to see the boundaries.
		self.frameCenterRow2=tk.Frame(self.centerFrame, bg=self.baseColor)
		#
		self.comboTimeDiff=tk.StringVar()
		self.comboTD=ttk.Combobox(self.frameCenterRow2, textvariable=self.comboTimeDiff, state="readonly", justify="center", width=15)
		# Revisit!! Next pass add Synodic Periods? to this list! Could be cool to advance by these durations too.
		self.comboTD["values"]=("SEC", "MIN", "HOUR", "DAY", "7 DAYS", "30 DAYS", "365 DAYS", "365.2563 DAYS")
		self.comboTD.current(3)
		self.comboTD.bind('<<ComboboxSelected>>', self.evaluateTDiff)
		self.comboTD.pack(anchor='w', side="left", padx=15)
		
		#
		###################################################
		# Initialize Graphics
		self.graphCanvas=tk.Canvas(self.bottomFrame, highlightthickness=0, bg=defaults.dlgDayChart, height = 100)
		# Hor scroll
		self.hsb=tk.Scrollbar(self.bottomFrame, orient="horizontal") # HSB first, so the pack() add the VSB widget after it.
		self.hsb.pack(side="bottom", fill="x")
		self.hsb.config(command=self.graphCanvas.xview)
		self.graphCanvas.config(xscrollcommand=self.hsb.set)
		# Vert scroll
		self.vsb=tk.Scrollbar(self.bottomFrame, orient="vertical")
		self.vsb.pack(side="right", fill="y")
		self.vsb.config(command=self.graphCanvas.yview)
		self.graphCanvas.config(yscrollcommand=self.vsb.set)
		# Don't PACK() the canvas until the scrollbars are expanded to "fill"
		self.graphCanvas.pack(expand=True, fill="both")
		# Capture window size changes
		self.graphCanvas.bind("<Configure>", self.on_resize)
		#
		####################################################
		#	Controls UI Layout - PACK()ing out in sequence
		self.frameCenterRow1.pack(fill="x", anchor='w', side="left")
		self.frameCenterRow2.pack(fill="x", anchor='sw', side="left")
		self.frameRightRow1.pack(fill="x")
		self.frameRightRow2.pack(fill="x")
		self.frameRightRow3.pack(fill="x")
		self.frameRightRow4.pack(fill="x")
		self.frameRightRow5.pack(fill="both", expand=True) # "<" button to the bottom
		self.frameLeftRow1.pack(fill="x")
		self.frameLeftRow2.pack(fill="x")
		self.frameLeftRow3.pack(fill="x")
		self.frameLeftRow4.pack(fill="x")
		self.frameLeftRow5.pack(fill="x")
		self.frameLeftRow6.pack(fill="both", expand=True) # ">" button to the bottom
		
	def on_resize(self,event):
		# The <Configure> event fires on initialization...
		self.canvasWidth, self.canvasHeight=event.width, event.height
		self.drawSomething(self)
	
	def userConfiguredContext(self, usr_context):
		#Receives the new CONTEXT object from the dialog box
		self.currentContext = self.usrContext
		self.master.title(self.currentContext.name)
		#update UI
		self.contextDate.set(self.currentContext.displayDateStr)
		self.contextLat.set(self.displayFormatLatLon(self.currentContext.latitude,"lat"))
		self.contextLon.set(self.displayFormatLatLon(self.currentContext.longitude,"lon"))
		self.currentJDate=self.currentContext.jdate
		self.currentDGEO=self.currentContext.dgeo
		self.evaluateTDiff()
		self.changePlanet() # --> Gets the new dataSet and calls DrawSomething

	def initialValues(self):
		#(name, dt, dtIsUTC=True tz_offset=None, applyDST=False, latitude=None, longitude=None, elevation=None)
		self.awareToday=datetime.now().astimezone()
		self.currentContext=context.context(name="Today", dt=self.awareToday) #context uses default lat/lon when not provided.
		self.master.title(self.currentContext.name)
		self.contextDate.set(self.currentContext.displayDateStr)
		self.contextLat.set(self.displayFormatLatLon(self.currentContext.latitude,"lat"))
		self.contextLon.set(self.displayFormatLatLon(self.currentContext.longitude,"lon"))
		self.currentJDate=self.currentContext.jdate
		self.currentDGEO=self.currentContext.dgeo
		self.evaluateTDiff()
		self.changePlanet()
	
	def evaluateTDiff(self,*args):
		# See self.comboTD["values"] ("SEC", "MIN", "HOUR", "DAY", "7 DAYS", "30 DAYS", "365 DAYS", "365.2563 DAYS")
		# Time differences in seconds
		self.deltaSeconds=(1,60,3600,86400,604800,2592000,31536000,31558149)
		#
		self.currentJDate=self.currentContext.jdate
		self.currentDGEO=self.currentContext.dgeo
		self.dt=self.currentContext.awareDT
		self.currentTD=timedelta(seconds=self.deltaSeconds[self.comboTD.current()])
		self.reducedDT=self.dt - self.currentTD
		self.advanceDT=self.dt + self.currentTD
	
	def subtractTime(self):
		self.focus()
		self.reducedDT=self.dt - self.currentTD
		self.adjustment=self.comboTimeDiff.get()
		self.currentContext=context.context(name=self.currentContext.name, dt=self.reducedDT) #self.currentContext=context.context(name="Adjusted  -" + self.adjustment, dt=self.reducedDT)
		#self.master.title(self.currentContext.name)
		self.contextDate.set(self.currentContext.displayDateStr) #update dialog label
		self.evaluateTDiff()
		self.changePlanet()

	def addTime(self):
		self.focus()
		self.advanceDT=self.dt + self.currentTD
		self.adjustment=self.comboTimeDiff.get()
		self.currentContext=context.context(name=self.currentContext.name, dt=self.advanceDT) #self.currentContext=context.context(name="Adjusted  +" + self.adjustment, dt=self.advanceDT)
		#self.master.title(self.currentContext.name)
		self.contextDate.set(self.currentContext.displayDateStr) #update dialog label
		self.evaluateTDiff()
		self.changePlanet()

	def changePlanet(self):
		# Build out the dataset for the selected planet
		# Planets 366, BUT outer planets need more records to fill available window space...
		# This feels wrong :-(
		samplesInnerPlanet, samplesOuterPlanet = 366, 366 #* 4
		self.innerJDate=self.currentJDate - (samplesInnerPlanet/2)
		self.outerJDate=self.currentJDate - (samplesOuterPlanet/2)
		self.dataSet=[]
		if self.selection.get() <= 4:
			for i in range(samplesInnerPlanet):
				self.placementData=placement.placementData(self.selection.get(), self.innerJDate, self.currentDGEO, i)
				self.dataSet.append(self.placementData)
				self.innerJDate +=1
			self.referencePlacement = self.dataSet[int(samplesInnerPlanet/2)]
		else:
			for i in range(samplesOuterPlanet):
				self.placementData=placement.placementData(self.selection.get(), self.outerJDate, self.currentDGEO, i)
				self.dataSet.append(self.placementData)
				self.outerJDate +=1
			self.referencePlacement = self.dataSet[int(samplesOuterPlanet/2)]
		self.getSignDetails(self.referencePlacement)
		self.drawSomething(self)
		#								** My Gordian Knot **
		#
		# Notes on flow-control for when forgotten: initialValues() initialization returns to main
		# and calls startCanvas(), which calls on_resize() - (even on) initialization - which calls drawSomething()
		# or...user interaction with comboGT call setGraphType(),
		# which also calls drawSomething(), to draw a different type of graph. While also...
		# ...user interaction with Radiobutton calls [to here] changePlanet(),
		# which again calls drawSomething(), to draw the graph for the new planet selected.
		#
		# User input from dialog box getData() is passed to userConfiguredContext() which makes basically the same
		# calls as initialValues(), just using the user provided values. Likewise, loadContext() reads a context object
		# from file and sends it to userConfiguredContext() to process/redraw the new graph saved data.
		#						***************************************
		
	def drawSomething(self, *args, **kwargs):
		self.horizonCanvas.delete("all")
		self.graphCanvas.delete("all")
		# Planet data should have already been recalculated for the right planet by now.
		#
		# Draw the horizon diagram
		constructdiagram.drawdiagram(self.dataSet, self.horizonCanvas)
		# Need the type of graph to pass as arg
		self.chosenGraph = self.comboGT.current() # Get the index, not the name.
		constructgraph.drawgraph(self.dataSet, self.graphCanvas, self.chosenGraph, self.maxScreen, self.themeInvisible.get())
	
	def showInvisible(self, *args, **kwargs):
		self.drawSomething(self)
	
	def setGraphType(self, *args, **kwargs):
		self.drawSomething(self)

	def getData(self, event=None):
		self.dlg=getcontextdata.datadialog(self, title="Get Event Data", context=self.currentContext)
		# Pass the retuned "new" user context back
		self.usrContext=self.dlg.newContext
		if self.usrContext:								#Canceling dialog returns a NONE object
			self.userConfiguredContext(self.usrContext)
	
	def displayFormatSign(self, eclon):
		signNum = int(eclon / 30)
		deg = eclon // 1
		minutes = 60 * (eclon - deg)
		seconds = round(60 * (minutes - int(minutes)),1)
		if seconds == 60:								#Revisit! - Feels like a hack, must be a better solution, fix displayFormatLatLon() also
			seconds = 0.0
			minutes += 1
			if debug: print("displayFormatSign arcsecond hack used")
		deg = int(deg) % 30
		minutes = int(minutes)
		#sign_keystroke = astrological_data.astroZodiacFont[0-11]
		string1 = "{deg}{d} "
		string2 = astrological_data.astroZodiacFont[signNum] #signGlyph
		string3 = " {minutes}{m} {seconds}{s}"
		return (string1.format(deg=deg,d=u"\u00b0"), string2 ,string3.format(minutes=minutes,m=u"\u0022",seconds=seconds,s=u"\u0027"))
		
		
	def displayFormatLatLon(self, val, type):
		#print(val)
		if type[:2].upper() == "LA":
			if val > 0: dir = "N"
			else: dir = "S"
		elif type[:2].upper() == "LO":
			if val > 0: dir = "E"
			else: dir = "W"
		else:
			if debug: print("**Error** Unknown return type requested")
			return 1
		val = math.fabs(val)
		deg = int(val)
		minutes = round(60 * (val - deg),6)								# round here fixes rounding drift for some values loss of 1/10 arcsec @ xxD 22' 0.0"
		seconds = functions.truncate(60 * (minutes - int(minutes)),1)
		deg = int(deg)
		minutes = int(minutes)
		string = "{deg}{d} {dir} {minutes}{m} {seconds}{s}"
		return string.format(deg=deg,d=u"\u00b0",dir=dir ,minutes=minutes,m=u"\u0022",seconds=seconds,s=u"\u0027")
		#
		#
	def getSignDetails(self, placementData):
		#sign_keystroke = astrological_data.astroZodiacFont[0-11]
		#planet_keystroke = astronomical_data.astroZodiacFont[0-6]
		# Clear prior values.
		report = ""
		self.solarOrientation.set("")
		self.solarRelationship.set("")
		self.sunPlacementText.delete('1.0', "end")
		self.signPlacementText.delete('1.0', "end")
		#
		triplicityLords = sign.getSign(placementData).signTriplicityLords
		lord_1 = astronomical_data.astroZodiacFont[triplicityLords[0]]
		lord_2 = astronomical_data.astroZodiacFont[triplicityLords[1]]
		lord_P = astronomical_data.astroZodiacFont[triplicityLords[2]]
		trpl = (lord_1, lord_2, lord_P)
		#
		self.signRuler.set(astronomical_data.astroZodiacFont[sign.getSign(placementData).signLord])
		self.triplicityLords.set(trpl)
		self.boundsLord.set(astronomical_data.astroZodiacFont[sign.getSign(placementData).signBoundLord])
		self.decanLord.set(astronomical_data.astroZodiacFont[sign.getSign(placementData).signDecanLord])
		
		dms_planet = self.displayFormatSign(placementData.eclon)
		signPlacement = (dms_planet)
		self.signPlacementText.insert("end", signPlacement[0], "regular")
		self.signPlacementText.insert("end", signPlacement[1], "astro")
		self.signPlacementText.insert("end", signPlacement[2])#, "regular") # Should the Min/Sec be small? Revisit later!
		if not placementData.isDirect:
			self.signPlacementText.insert("end", " " + astronomical_data.astroZodiacFont[-1], "retro")
		#
		# Chart Sect
		self.chartSect.set(astrological_data.chartSect[placementData.sect])
		#
		# Sun's placement, if we're not already looking at the SUN
		if not "SUN" == placementData.planetName.upper():
			dms_sun = self.displayFormatSign(placementData.degPlacementSUN)
			sunPlacement = (dms_sun)
			self.sunPlacementText.insert("end", sunPlacement[0], "regular")
			self.sunPlacementText.insert("end", sunPlacement[1], "astro")
			self.sunPlacementText.insert("end", sunPlacement[2])#, "regular") # Should the Min/Sec be small? Revisit later!
			self.sunPlacementText.insert("end", " " + astronomical_data.astroZodiacFont[0], "astro")
			#
			# Planets placement relative to the SUN, if NOT the SUN
			if placementData.isOriental:
				self.solarOrientation.set("ORIENTAL")
			else:
				self.solarOrientation.set("OCCIDENTAL")
			#
			# SUN can't be under its own beams!!!
			if placementData.isUnderBeams:
				self.solarRelationship.set("UNDER the BEAMS")
				if placementData.isCombust:
					self.solarRelationship.set("COMBUST")
					if placementData.isCazimi:
						self.solarRelationship.set("CAZIMI")
		#
		#---# End of SUN's exclusion.
		#
		# Display Dignity values
		r = sign.getSign(placementData).isLord
		e = sign.getSign(placementData).isExalted
		f = sign.getSign(placementData).isInFall
		d = sign.getSign(placementData).isInDetriment
		if e: report = "EXALTED"
		if f: report = "In FALL"
		if r:
			report = "RULER"
			if e: report = "EXALTED " + report
		if d:
			report = "In DETRIMENT"
			if f: report = "In FALL and " + report
		#
		t = sign.getSign(placementData).hasTriplicity
		b = sign.getSign(placementData).isBoundLord
		de = sign.getSign(placementData).isDecanLord
		if t:
			plusT = " TRIPLICITY "
		if b:
			plusB = " BOUND "
		if de:
			plusDE = " DECAN "
		#
		if t or b or de:
			if report: report = report + " with dignity by "
			else: report = "dignity by "
		if t: report = report + plusT
		if b: report = report + plusB
		if de: report = report + plusDE
		#
		self.dignity.set(report)
	
	def loadMenubar(self, window):
		self.appMenus = tk.Menu(window,tearoff=0)	# Create menubar object
		# Add menus to menubar , tearoff=0)?
		self.filemenu = tk.Menu(self.appMenus,tearoff=0)
		self.appMenus.add_cascade(label="File", menu = self.filemenu)
		self.helpmenu = tk.Menu(self.appMenus,tearoff=0)
		self.appMenus.add_cascade(label="Help", menu = self.helpmenu)
		
		# File Menuitems
		#Load menu flyout - enumerate files that can be loaded
		self.loadmenu = tk.Menu(self.filemenu,tearoff=0)
		self.filemenu.add_cascade(label="Load", menu = self.loadmenu)
		# Rest of the file menu items
		self.filemenu.add_command(label="Modify", command = self.getData)
		self.filemenu.add_command(label="Save", command = self.saveContext)
		self.filemenu.add_command(label="Options", command = self.applicationOptions)
		self.filemenu.add_separator()
		self.filemenu.add_command(label="Exit", command = self.applicationClose)

		# Help Menuitems
		self.helpmenu.add_command(label="About " + se_exec.applicationName, command = self.getAbout)

		# Add menubar to window
		window.config(menu = self.appMenus)
	
	def getAbout(self, event=None):
		about.aboutdialog(self, title="Application Inormation")
	
	
	def getLoadableFile(self):
		# Clean up the existing menuitems
		self.loadmenu.delete(0, self.loadmenu.index("end"))
		self.foundFiles = []
		# Populate list of .context files available to load
		for fn in os.listdir(self.filesys.saveLocation()):
			if fn.endswith(".context"): self.foundFiles.append(fn)
		
		for file in self.foundFiles:
			self.loadmenu.add_command(label=file, command=lambda menuitem=file: self.loadContext(menuitem))
	
	
	def loadContext(self, filename):
		fileData = self.filesys.loadFromFile(filename)
		if fileData:
			self.usrContext = fileData
			self.userConfiguredContext(self.usrContext)
		
		
	def saveContext(self):
		saveStatus = False
		saveStatus = self.filesys.saveToFile(self.currentContext)
		# Save needs to reload the menubar to update the list of available files to load.
		if saveStatus:
			self.getLoadableFile()


	def applicationOptions(self):
		msgText = "Dialog Comming Soon!\n\nIn the meanwhile see defaults.py for options that can be set."
		self.optionsPlaceholder = tk.messagebox.showinfo(title="Options", message=msgText)

	def testForFont(self):
		# Test for ASTRO font availability
		isAstro = False
		for name in sorted(tkinter.font.families()):
			if name.upper() == "ASTRO":
				isAstro = True
			#print(name)
		if not isAstro:
			msgText = "ASTRO.ttf not found.\nInstall it from .\\res directory."
			self.fontWarning = tk.messagebox.showwarning(title="Astro Font Not Installed", message=msgText)
			self.grab_set()
			self.grab_release()

	def applicationClose(self):
		self.appWindow.destroy()
