#!/bin/sh
curl -s "ftp://www.astro.com/pub/jplfiles/md5sums.txt" | grep de431.eph$ > md5sums.txt
# If you really want **ALL** of the JPL files uncomment but go get dinner, its going to be a while.
#curl -s "ftp://www.astro.com/pub/jplfiles/md5sums.txt" | grep .eph$ > md5sums.txt

file="$PWD/md5sums.txt"

echo This may take a very long time...
while read -r f1 f2
do
	curl -# --ftp-pasv "ftp://www.astro.com/pub/jplfiles/$f2" --output $f2
done <"$file"
rm "$file"
