# Copyright (C) 2020  PracticalEndeavors.com, Alexander A. Genna
# GNU General Public License, version 2, see observer.py for license information.
import defaults
import astrological_data
import astronomical_data

class getSign:
	def __init__(self, placementObject):
		'''	Evaluate sign placement values							 							'''
		''' properties:																			'''
		''' sect, signNum, signName, element, modality, signLord, signExalted, signFallen, 		'''
		''' signDetriment, signTriplicityLords, signBoundLord, signDecanLord, isLord, isExalted	'''
		''' isInFall, isInDetriment																'''
		'''																						'''
		''' methods:																			'''
		'''	getSign, getSignName, getSignElementName, getSignModeName, getTriplicityLords, 		'''
		''' getBoundLord, getDecanLord,getSign_PlanetIsLord, getSign_PlanetIsExalted, 			'''
		''' getSign_PlanetInFall, getSign_PlanetInDet											'''
		#
		self.output = 0	# 0 - False | 1 - True
		self.debug = 0	# 0 - False | 1 - True
		#
		# These functions require the following values to be defined/imported;
		# function: getTriplicityLords(), getBoundLord() -- sect
		# function: getBoundLord()						 -- boundType
		#
		self.eclon = placementObject.eclon
		self.sect = placementObject.sect
		self.planetID = astronomical_data.classicBodies.index(placementObject.planetName.upper())
		signNum = int(self.eclon / 30)
		# Get user default selection for type of Terms/Bounds to use
		self.boundType = defaults.BOUNDS
		#
		# Dispositors
		self.signName = self.getSignName(signNum)
		self.element = self.getSignElementName(signNum)
		self.modality = self.getSignModeName(signNum)
		self.signLord = astrological_data.sign_rul[signNum]
		self.signExalted = astrological_data.sign_ex[signNum]
		self.signFallen = astrological_data.sign_fa[signNum]
		self.signDetriment =astrological_data.sign_det[signNum]
		self.signTriplicityLords = self.getTriplicityLords(self.eclon, self.sect)
		self.signBoundLord = self.getBoundLord(self.eclon, self.sect)
		self.signDecanLord = self.getDecanLord(self.eclon)
		# Dignity, returns as boolean
		self.isLord = self.isPlanetLord(self.eclon, self.planetID)
		self.isExalted = self.isPlanetExalted(self.eclon, self.planetID)
		self.isInFall = self.isPlanetInFall(self.eclon, self.planetID)
		self.isInDetriment = self.isPlanetInDet(self.eclon, self.planetID)
		self.hasTriplicity = self.hasTriplicityDignity(self.planetID, self.signTriplicityLords)
		self.isBoundLord = self.hasBoundDignity(self.planetID, self.signBoundLord)
		self.isDecanLord = self.hasDecanDignity(self.planetID, self.signDecanLord)
	#
	def getSignName(self, eclon):
		signNum = int(eclon / 30)
		# Send sign as index value - 0 Aries, 1 Taurus...
		signName = astrological_data.zodiac[signNum]
		return signName
	#
	# Functions all expect ecliptic longitude as sole argument
	#
	def getSignElementName(self, eclon):
		signNum = int(eclon / 30)
		if signNum in (0, 4, 8):
			signElementName = astrological_data.elements[0]
		elif signNum in (1, 5, 9):
			signElementName = astrological_data.elements[1]
		elif signNum in (2, 6, 10):
			signElementName = astrological_data.elements[2]
		elif signNum in (3, 7, 11):
			signElementName = astrological_data.elements[3]
		else:
			print("Problem in getSignElementName()")
			return -1
		return signElementName
	#
	def getSignModeName(self, eclon):
		signNum = int(eclon / 30)
		if signNum in (0, 3, 6, 9):
			signModeName = astrological_data.modes[0]
		elif signNum in (1, 4, 7, 10):
			signModeName = astrological_data.modes[1]
		elif signNum in (2, 5, 8, 11):
			signModeName = astrological_data.modes[2]
		else:
			print("Problem in getSignModeName()")
			return -1
		return signModeName
	#
	def getTriplicityLords(self, eclon, sect):
		# Function requires SECT
		#get sect - SUN above or below ASC/DSC axis
		#get sign element
		signElementName = self.getSignElementName(eclon)
		# print(signElementName)
		if "FIRE" == signElementName:
			if astrological_data.DAY == sect:
				signTriplicityLords = astrological_data.trip_fire_day
			elif astrological_data.NIGHT == sect:
				signTriplicityLords = astrological_data.trip_fire_night
		elif "EARTH" == signElementName:
			if astrological_data.DAY == sect:
				signTriplicityLords = astrological_data.trip_earth_day
			elif astrological_data.NIGHT == sect:
				signTriplicityLords = astrological_data.trip_earth_night
		elif "AIR" == signElementName:
			if astrological_data.DAY == sect:
				signTriplicityLords = astrological_data.trip_air_day
			elif astrological_data.NIGHT == sect:
				signTriplicityLords = astrological_data.trip_air_night
		elif "WATER" == signElementName:
			if astrological_data.DAY == sect:
				signTriplicityLords = astrological_data.trip_water_day
			elif astrological_data.NIGHT == sect:
				signTriplicityLords = astrological_data.trip_water_night
		else:
			print("Problem in getTriplicityLords()")
			return -1
		return signTriplicityLords
	#
	def getBoundLord(self, eclon, sect):
		# Function requires SECT
		#bounds according to whom?
		if self.boundType == 0:
			boundSet = astrological_data.bounds_egyptian				#if self.output: print("Using Egyptian Bounds")
		elif self.boundType == 1 and sect == astrological_data.DAY:
			boundSet = astrological_data.bounds_ptolemy_day				#if self.output: print("Using Ptolemy's (Day) Bounds")
		elif self.boundType == 1 and sect == astrological_data.NIGHT:
			boundSet = astrological_data.bounds_ptolemy_night			#if self.output: print("Using Ptolemy's (Night) Bounds")
		else:
			print("Problem in getBoundLord()")
			return -1
		#
		signNum = int(eclon / 30)
		signStart = signNum * 30
		signDeg = eclon - signStart
		signBounds = boundSet[signNum]
		# e.g. signBounds = ((0, 4),(6, 5),(13, 2),(20, 3),(27, 6))
		boundLord = -1 # if boundLord returns negative something's wrong. Rx in the astroFont array.
		for bound in signBounds:
			if signDeg >= bound[0]:
					boundLord = bound[1]
			else:
				break
		return boundLord
	#
	def getDecanLord(self, eclon):
		signNum = int(eclon / 30)
		signStartDeg = signNum * 30
		degPlacement = eclon - signStartDeg
		if degPlacement < 10:
			decanLord = astrological_data.decan[signNum][0]
		elif degPlacement >= 20:
			decanLord = astrological_data.decan[signNum][2]
		else:
			decanLord = astrological_data.decan[signNum][1]
		return decanLord
	#
	def isPlanetLord(self, eclon, pId):
		signNum = int(eclon / 30)
		r = astrological_data.sign_rul[signNum]
		if r == pId: lord = True
		else: lord =False
		return lord
	#
	def isPlanetExalted(self, eclon, pId):
		signNum = int(eclon / 30)
		e = astrological_data.sign_ex[signNum]
		# -1 returned for no lord
		if	e < 0: lord = False # no exaltation here
		else:
			if e == pId: lord = True
			else: lord =False
		return lord
	#
	def isPlanetInFall(self, eclon, pId):
		signNum = int(eclon / 30)
		f = astrological_data.sign_fa[signNum]
		# -1 returned for no lord
		if	f < 0: lord =False # no exaltation here
		else:
			if f == pId: lord = True
			else: lord =False
		return lord
	#
	def isPlanetInDet(self, eclon, pId):
		signNum = int(eclon / 30)
		d = astrological_data.sign_det[signNum]
		if d == pId: lord = True
		else: lord =False
		return lord

	def hasTriplicityDignity(self, pId, triplicityLords):
		lord = False
		for l in triplicityLords:
			if pId == l: lord = True
			else: lord = False
		return lord
	
	def hasBoundDignity(self, pId, boundLord):
		if pId == boundLord: lord = True
		else: lord = False
		return lord
	
	def hasDecanDignity(self, pId, decanLord):
		if pId == decanLord: lord = True
		else: lord = False
		return lord
