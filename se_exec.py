# Copyright (C) 2020  PracticalEndeavors.com, Alexander A. Genna
# GNU General Public License, version 2, see observer.py for license information.
debug = 0	# 0 - False | 1 - True # macOS list objects from shared library [nm -gU lib/sweastronomy.so]
import sys
import os
import platform
CWD = os.getcwd()
sys.path.append( CWD + '/lib/')
applicationName = "Astrologic Observer"
applicationVersion = "version: 1.00"
#
# What OS are we running on?
osName = platform.system()	# Returns are Linux: Linux, MacOS: Darwin, Windows: Windows
if osName.upper() == "WINDOWS": sepChar = ";" # semicolon
else: sepChar = ":" # colon
SE_EPHE_PATH = CWD + "/ephe/jplfiles/" + sepChar + CWD + "/ephe/" # Set JPL first in case someone actually download one
#
# These file will need to be downloaded from NASA/JPL - They are Gigantic!
# For unix/macos see curl shell script in "./ephe/jplfiles/"; Sorry Microsoft users.
# DE432 - c.2014 - Covers JED 2287184.5, (1549 DEC 21) to JED 2688976.5, (2650 JAN 25)
# DE431 - c.2013 - Covers JED -3100015.5, (-13200 AUG 15) to JED 8000016.5, (17191 MAR 15).
# Note:	Python DateTime date range BEGINS at the year 1 CE. We are limited by this
# 		regardless of ephemeris support for BC dates.
SE_JPL_FILE = "de431.eph"
if debug: print("Ephemeris path: ",SE_EPHE_PATH, "JPL Filename:", SE_JPL_FILE)

#### Useing pyswisseph (swisseph.cpython-38-darwin.so, swisseph.cp38-win32.pyd)
import swisseph as swe 				# https://astrorigin.com/pyswisseph/pydoc/
from swisseph import contrib as swh	# https://astrorigin.com/pyswisseph/pydoc/swisseph.contrib.html
swe.set_ephe_path(SE_EPHE_PATH)
swe.set_jpl_file(SE_JPL_FILE)


def printSweVer():
	return print("Using Swiss Ephemeris\tversion:",swe.version)
#
#swe.FLG_JPLEPH = 1
#swe.FLG_SWIEPH = 2
#swe.FLG_SPEED = 256
#defaultSEFLG = swe.FLG_SPEED + swe.FLG_SWIEPH
