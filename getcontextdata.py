# Copyright (C) 2020  PracticalEndeavors.com, Alexander A. Genna
# GNU General Public License, version 2, see observer.py for license information.
import tkinter as tk
import tkinter.ttk as ttk
from tkinter import messagebox
import defaults
import functions
import context
from datetime import timedelta, datetime, tzinfo, timezone

class datadialog(tk.Toplevel):
	def __init__(self, received_window, title = None, context=None):
		tk.Toplevel.__init__(self, received_window)
		self.baseColor = defaults.dlgColor
		self.configure(background=self.baseColor)
		self.transient(received_window)
		if title: self.title(title)
		self.received_window = received_window
		self.initial_focus = self.body(self.received_window)
		self.grab_set()
		if not self.initial_focus: self.initial_focus = self
		self.protocol("WM_DELETE_WINDOW", self.cancel)
		self.geometry("+%d+%d" % (received_window.winfo_rootx()+50, received_window.winfo_rooty()+50))
		self.initial_focus.focus_set()
		
		if not context: self.newContext=None
		else: self.newContext=context
		self.usrName = None
		self.usrLon = None
		self.usrLat = None
		self.usrElev = None
		self.usrYear = None
		self.usrMonth = None
		self.usrDay = None
		self.usrHour = None
		self.usrMin = None
		self.usrSec = None
		self.usrTZ = None
		self.timeIsDST = False
		
		self.wait_window(self)


	def body(self, dialogWindow):
		self.resizable(False, False)	#Opens as independent window, not a "tab"
		style = ttk.Style()
		style.configure('TLabel', background=self.baseColor)
		style.configure('TEntry', borderwidth=0)
		style.map('TEntry', background=[('disabled', self.baseColor)], relief=[('disabled', 'ridge')])
		style.configure('TButton', background=self.baseColor)
		style.configure('TCheckbutton', background=self.baseColor)
		
		self.dialogFrame=tk.Frame(self, padx=15, pady=5, bg=self.baseColor)
		self.keyEntry = (self.register(self.testEntry),'%d', '%i', '%P', '%s', '%S', '%v', '%V', '%W')
		
		#################
		# LocationFrame
		self.locationFrame=tk.Frame(self.dialogFrame, bg=self.baseColor)
		
		self.locName = tk.StringVar()
		self.locLat = tk.StringVar()
		self.locLon = tk.StringVar()
		self.locElev = tk.StringVar()
		
		self.dst = tk.BooleanVar()
		self.dst.set(False)
		
		self.labelLoc = ttk.Label(self.locationFrame, text="Location of Event")
		self.labelLocName = ttk.Label(self.locationFrame, text="Location Name:")
		self.labelLocLat = ttk.Label(self.locationFrame, text="Latitude:")
		self.labelLocLon = ttk.Label(self.locationFrame, text="Longitude:")
		self.labelLocElev = ttk.Label(self.locationFrame, text="Elevation [m]:")
		
		lfWidth=18
		self.inputLocName = ttk.Entry(self.locationFrame, textvariable=self.locName, width=lfWidth,name='inputLocName')
		self.inputLocLat = ttk.Entry(self.locationFrame, textvariable=self.locLat, width=lfWidth, name='inputLocLat', validate = 'key', validatecommand = self.keyEntry)
		self.inputLocLon = ttk.Entry(self.locationFrame, textvariable=self.locLon, width=lfWidth, name='inputLocLon', validate = 'key', validatecommand = self.keyEntry)
		#
		# DMS Accents
		self.DLabel1=tk.Label(self.locationFrame, text=u"\u00b0", bg=self.baseColor)
		self.MLabel1=tk.Label(self.locationFrame, text=u"\u0022", bg=self.baseColor)
		self.SLabel1=tk.Label(self.locationFrame, text=u"\u0027", bg=self.baseColor)
		
		self.DLabel2=tk.Label(self.locationFrame, text=u"\u00b0", bg=self.baseColor)
		self.MLabel2=tk.Label(self.locationFrame, text=u"\u0022", bg=self.baseColor)
		self.SLabel2=tk.Label(self.locationFrame, text=u"\u0027", bg=self.baseColor)
		
		# Alt Entry for DMS entry Latitude
		self.altLatDeg = tk.StringVar()
		self.altLatNS = tk.StringVar()
		self.altLatMin = tk.StringVar()
		self.altLatSec = tk.StringVar()
		self.enableAltLat = tk.BooleanVar()
		self.enableAltLat.set(False)

		self.inputAltLatDeg = ttk.Entry(self.locationFrame, textvariable=self.altLatDeg, name='inputAltLatDeg', validate = 'key', validatecommand = self.keyEntry, state="disabled", width=3)
		self.inputAltLatNS = ttk.Entry(self.locationFrame, textvariable=self.altLatNS, name='inputAltLatNS', validate = 'key', validatecommand = self.keyEntry, state="disabled", width=2)
		self.inputAltLatMin = ttk.Entry(self.locationFrame, textvariable=self.altLatMin, name='inputAltLatMin', validate = 'key', validatecommand = self.keyEntry, state="disabled", width=2)
		self.inputAltLatSec = ttk.Entry(self.locationFrame, textvariable=self.altLatSec, name='inputAltLatSec', validate = 'key', validatecommand = self.keyEntry, state="disabled", width=4)
		self.enableAltLatChkBtn = ttk.Checkbutton(self.locationFrame, text="Lat DMS", variable=self.enableAltLat, onvalue=True, command=self.useAltLat)
		#
		# Alt Entry for DMS entry Longitude
		self.altLonDeg = tk.StringVar()
		self.altLonEW = tk.StringVar()
		self.altLonMin = tk.StringVar()
		self.altLonSec = tk.StringVar()
		self.enableAltLon = tk.BooleanVar()
		self.enableAltLon.set(False)
		
		self.inputAltLonDeg = ttk.Entry(self.locationFrame, textvariable=self.altLonDeg, name='inputAltLonDeg', validate = 'key', validatecommand = self.keyEntry, state="disabled", width=3)
		self.inputAltLonEW = ttk.Entry(self.locationFrame, textvariable=self.altLonEW, name='inputAltLonEW', validate = 'key', validatecommand = self.keyEntry, state="disabled", width=2)
		self.inputAltLonMin = ttk.Entry(self.locationFrame, textvariable=self.altLonMin, name='inputAltLonMin', validate = 'key', validatecommand = self.keyEntry, state="disabled", width=2)
		self.inputAltLonSec = ttk.Entry(self.locationFrame, textvariable=self.altLonSec, name='inputAltLonSec', validate = 'key', validatecommand = self.keyEntry, state="disabled", width=4)
		self.enableAltLonChkBtn = ttk.Checkbutton(self.locationFrame, text="Lon DMS", variable=self.enableAltLon, onvalue=True, command=self.useAltLon)
		#
		self.inputLocElev = ttk.Entry(self.locationFrame, textvariable=self.locElev, width=lfWidth, name='inputLocElev', validate = 'key', validatecommand = self.keyEntry)

		#################
		# DateTimeFrame
		self.dateTimeFrame=tk.Frame(self.dialogFrame, bg=self.baseColor)
		
		self.Hour = tk.StringVar()
		self.Min = tk.StringVar()
		self.Sec = tk.StringVar()
		self.Year = tk.StringVar()
		self.Month = tk.StringVar()
		self.Day = tk.StringVar()
		self.TZ = tk.StringVar()
		
		self.labelTime = ttk.Label(self.dateTimeFrame, text="Time of Event")
		
		self.dayFrame=tk.Frame(self.dateTimeFrame, bg=self.baseColor)
		#
		self.labelDate = ttk.Label(self.dayFrame, text="M:D:YYYY:", width=9)
		self.inputMonth = ttk.Entry(self.dayFrame, textvariable=self.Month, width=5, name='inputMonth', validate = 'key', validatecommand = self.keyEntry)
		self.inputDay = ttk.Entry(self.dayFrame, textvariable=self.Day, width=5, name='inputDay', validate = 'key', validatecommand = self.keyEntry)
		self.inputYear = ttk.Entry(self.dayFrame, textvariable=self.Year, width=5, name='inputYear', validate = 'key', validatecommand = self.keyEntry)

		self.timeFrame=tk.Frame(self.dateTimeFrame, bg=self.baseColor)
		#
		self.labelHour = ttk.Label(self.timeFrame, text="HR:MM:SS:", width=9)
		self.inputHour = ttk.Entry(self.timeFrame, textvariable=self.Hour, width=5, name='inputHour', validate = 'key', validatecommand = self.keyEntry)
		self.inputMin = ttk.Entry(self.timeFrame, textvariable=self.Min, width=5, name='inputMin', validate = 'key', validatecommand = self.keyEntry)
		self.inputSec = ttk.Entry(self.timeFrame, textvariable=self.Sec, width=5, name='inputSec', validate = 'key', validatecommand = self.keyEntry)
		
		
		self.utcFrame=tk.Frame(self.dateTimeFrame, bg=self.baseColor)
		#
		self.labelTz = ttk.Label(self.utcFrame, text="Offset UTC:", width=9)
		self.inputTz = ttk.Entry(self.utcFrame, textvariable=self.TZ, width=5, name='inputTz', validate = 'key', validatecommand = self.keyEntry)
		self.dstChkBtn = ttk.Checkbutton(self.utcFrame, text="DST", variable=self.dst, onvalue=True, command=self.chkDST)

		#################
		# Controls
		self.buttonFrame = tk.Frame(self, bg=self.baseColor)
		self.editCurrentButton = ttk.Button(self.buttonFrame, text="Edit Current", width=10, command=self.editContext)
		self.okButton = ttk.Button(self.buttonFrame, text="OK", width=10, command=self.ok, state='normal')
		self.cancelButton = ttk.Button(self.buttonFrame, text="Cancel", width=10, command=self.cancel, default="active")
		self.bind('<Return>', self.ok)
		self.bind('<Escape>', self.cancel)
		
		#
		# Location Layout
		self.dialogFrame.grid(row=0,column=0)
		#
		# Left hand Frame
		self.locationFrame.grid(row=0,column=0, padx=15)
		#
		self.labelLoc.grid(row=0,column=0, sticky='w')
		self.labelLocName.grid(row=1,column=0, sticky='w')
		self.inputLocName.grid(row=1,column=1, columnspan=7, sticky='w e')
		#
		# Entry Latitude
		self.labelLocLat.grid(row=2,column=0, sticky='w')
		self.inputLocLat.grid(row=2,column=1, columnspan=7, sticky='w e')
		# Alt Entry Latitude
		self.enableAltLatChkBtn.grid(row=3,column=0, sticky='w')
		self.inputAltLatDeg.grid(row=3,column=1, sticky='w e')
		self.DLabel1.grid(row=3,column=2, sticky='w')
		self.inputAltLatNS.grid(row=3,column=3, padx=6, sticky='w e')
		self.inputAltLatMin.grid(row=3,column=4, sticky='w e')
		self.MLabel1.grid(row=3,column=5, sticky='w')
		self.inputAltLatSec.grid(row=3,column=6, sticky='w e')
		self.SLabel1.grid(row=3,column=7, sticky='w')
		#
		#Entry Longitude
		self.labelLocLon.grid(row=4,column=0, sticky='w')
		self.inputLocLon.grid(row=4,column=1, columnspan=7, sticky='w e')
		# Alt Entry Longitude
		self.enableAltLonChkBtn.grid(row=5,column=0, sticky='w')
		self.inputAltLonDeg.grid(row=5,column=1, sticky='w e')
		self.DLabel2.grid(row=5,column=2, sticky='w')
		self.inputAltLonEW.grid(row=5,column=3, padx=6, sticky='w e')
		self.inputAltLonMin.grid(row=5,column=4, sticky='w e')
		self.MLabel2.grid(row=5,column=5, sticky='w')
		self.inputAltLonSec.grid(row=5,column=6, sticky='w e')
		self.SLabel2.grid(row=5,column=7, sticky='w')
		#
		self.labelLocElev.grid(row=6,column=0, sticky='w')
		self.inputLocElev.grid(row=6,column=1, columnspan=7, sticky='w e')
		#
		# Right hand frame
		# row 0
		self.dateTimeFrame.grid(row=0, column=1, padx=15 ,sticky='nw')
		self.labelDate.grid(row=0,column=0, sticky='w')
		#
		# row 1
		self.dayFrame.grid(row=1,column=0, sticky='w')
		#
		self.labelTime.grid(row=0,column=0, sticky='w')
		self.inputMonth.grid(row=0,column=1, sticky='e')
		self.inputDay.grid(row=0,column=2, sticky='e')
		self.inputYear.grid(row=0,column=3, sticky='e')
		
		# row 2
		self.timeFrame.grid(row=2,column=0, sticky='w')
		#
		self.labelHour.grid(row=0,column=0, sticky='w')
		self.inputHour.grid(row=0,column=1, sticky='e')
		self.inputMin.grid(row=0,column=2, sticky='e')
		self.inputSec.grid(row=0,column=3, sticky='e')
		
		# row 3
		self.utcFrame.grid(row=3,column=0, sticky='w')
		#
		self.labelTz.grid(row=0,column=0)
		self.inputTz.grid(row=0,column=1)
		self.dstChkBtn.grid(row=0,column=2, sticky='w')
		#
		#########
		# Buttons
		self.buttonFrame.grid(sticky='s')
		self.editCurrentButton.grid(row=0,column=1, padx=100, pady=5)
		self.okButton.grid(row=0,column=2, padx=5, pady=5)
		self.cancelButton.grid(row=0,column=3, padx=5, pady=5)
	
	def useAltLon(self):
		if self.enableAltLon.get():
			# Lon - Clear decimal degree input fields and disable
			self.locLon.set("")
			self.inputLocLon.config(state='disable')
			# Enable the DMS fields
			self.inputAltLonDeg.config(state='normal')
			self.inputAltLonEW.config(state='normal')
			self.inputAltLonMin.config(state='normal')
			self.inputAltLonSec.config(state='normal')
		else:
			# Lon - Clear DMS input fields
			self.altLonDeg.set("")
			self.altLonEW.set("")
			self.altLonMin.set("")
			self.altLonSec.set("")
			self.inputAltLonDeg.config(state='disable')
			self.inputAltLonEW.config(state='disable')
			self.inputAltLonMin.config(state='disable')
			self.inputAltLonSec.config(state='disable')
			# Enable the decimal degree input field
			self.inputLocLon.config(state='normal')
			
	def useAltLat(self):
		if self.enableAltLat.get():
			# Lat - Clear decimal degree input field
			self.locLat.set("")
			self.inputLocLat.config(state='disable')
			# Enable the DMS fields
			self.inputAltLatDeg.config(state='normal')
			self.inputAltLatNS.config(state='normal')
			self.inputAltLatMin.config(state='normal')
			self.inputAltLatSec.config(state='normal')
		else:
			# Lat - Clear DMS input fields
			self.altLatDeg.set("")
			self.altLatNS.set("")
			self.altLatMin.set("")
			self.altLatSec.set("")
			self.inputAltLatDeg.config(state='disable')
			self.inputAltLatNS.config(state='disable')
			self.inputAltLatMin.config(state='disable')
			self.inputAltLatSec.config(state='disable')
			# Enable the decimal degree input field
			self.inputLocLat.config(state='normal')
	
	def editContext(self):
		# Load existing context data for edit as new
		context = self.newContext
		dt = context.awareDT
		old_name = context.name
		old_lat = context.latitude
		old_lon = context.longitude
		old_elev = context.elevation
		old_year = dt.year
		old_month = dt.month
		old_day = dt.day
		old_hour = dt.hour
		old_min = dt.minute
		old_sec = dt.second
		old_tz = context.tz_offset
		# Big disconnect with DST, since it is basically a set of legislated rules
		# We can get accurate TZ-Offsett from UTC, but no way to know if DST was / should have been set or not.
		old_dst = context.applyDST
		# Update Variables
		self.locName.set(old_name)
		
		#
		if not self.enableAltLat.get():
			self.locLat.set(functions.truncate(old_lat,6))
		else:
			dmsLat = functions.returnDMSLat(old_lat)
			self.altLatDeg.set(str(dmsLat[0]))
			self.altLatNS.set(str(dmsLat[1]))
			self.altLatMin.set(str(dmsLat[2]))
			self.altLatSec.set(str(functions.truncate(dmsLat[3],1)))
		#
		if not self.enableAltLon.get():
			self.locLon.set(functions.truncate(old_lon,6))
		else:
			dmsLon = functions.returnDMSLon(old_lon)
			self.altLonDeg.set(str(dmsLon[0]))
			self.altLonEW.set(str(dmsLon[1]))
			self.altLonMin.set(str(dmsLon[2]))
			self.altLonSec.set(str(functions.truncate(dmsLon[3],1)))
		#
		self.locElev.set(old_elev)
		self.Year.set(old_year)
		self.Month.set(old_month)
		self.Day.set(old_day)
		self.Hour.set(old_hour)
		self.Min.set(old_min)
		self.Sec.set(old_sec)
		self.TZ.set(old_tz)
		# Do NOT reset Daylight Savings time checkbox
		# The Timezone offset itself has been modified already this would
		#cause a user-feedback loop drifting the offset on each edit.
		self.dst.set(False)
	
	def chkDST(self):
		self.timeIsDST = self.dst.get()	#Its either True or False...
		#print ("variable is", self.dst.get())

	def cancel(self, event=None):
		# put focus back to the received_window window
		self.received_window.focus_set()
		self.destroy()

	def ok(self, event=None):
		self.validate()
		self.withdraw()		#if self.dataValidated: conditional leaves the dialog unusable - Revisit!!
		self.update_idletasks()
		self.apply()
		self.cancel() # destroy window

	def validate(self):
		self.dataValidated = False
		#
		self.usrName = self.inputLocName.get()
		if not self.usrName:
			self.usrName = "Unnamed User Data"
		#
		if not self.enableAltLat.get():
			self.locLat = self.inputLocLat.get()
		else:
			d1 = self.inputAltLatDeg.get()
			h1 = self.inputAltLatNS.get()
			m1 = self.inputAltLatMin.get()
			s1 = self.inputAltLatSec.get()
		
		if not self.enableAltLon.get():
			self.locLon = self.inputLocLon.get()
		else:
			d2 = self.inputAltLonDeg.get()
			h2 = self.inputAltLonEW.get()
			m2 = self.inputAltLonMin.get()
			s2 = self.inputAltLonSec.get()
		
		self.locElev = self.inputLocElev.get()
		self.Year = self.inputYear.get()
		self.Month = self.inputMonth.get()
		self.Day = self.inputDay.get()
		self.Hour = self.inputHour.get()
		self.Min = self.inputMin.get()
		self.Sec = self.inputSec.get()
		self.TZ = self.inputTz.get()
		#
		try:
			# DGEO
			if not self.enableAltLat.get():
				if self.locLat.strip():
					self.usrLat = float(self.locLat)
				else:
					raise requiredParameterMissing
			else:
				if '' == s1:
					s1 = '0'				# Allow null input of arcsec
				if '' == m1:
					m1 = '0'				# Allow null input of arcmin
				if d1 and (h1 and (m1 and s1)):
					self.usrLat = functions.returnDecimalDegree(d1, h1, m1, s1)
				else:
					raise requiredParameterMissing
			
			if not self.enableAltLon.get():
				if self.locLon.strip():
					self.usrLon = float(self.locLon)
				else:
					raise requiredParameterMissing
			else:
				if '' == s2:
					s2 = '0'				# Allow null input of arcsec
				if '' == m2:
					m2 = '0'				# Allow null input of arcmin
				if d2 and (h2 and (m2 and s2)):
					self.usrLon = functions.returnDecimalDegree(d2, h2, m2, s2)
				else:
					raise requiredParameterMissing
				
			if self.locElev.strip():
				self.usrElev = int(self.locElev)
			else:
				raise requiredParameterMissing
			#
			# Datetime Object Requirements
			if self.TZ.strip():
				self.usrTZ = functions.truncate(float(self.TZ),1)
			else:
				raise requiredParameterMissing
				
			if self.Year.strip():
				self.usrYear = int(self.Year)
			else:
				raise requiredParameterMissing
				
			if self.Month.strip():
				self.usrMonth = int(self.Month)
			else:
				raise requiredParameterMissing
				
			if self.Day.strip():
				self.usrDay = int(self.Day)
				if int(self.Month) == 2 and int(self.Year) % 4 == 0: # is a leap year
					if self.usrDay > 29:
						raise invalidParameter
				elif int(self.Month) == 2 and int(self.Year) % 4 > 0: # not a leap year
						if self.usrDay > 28:
							raise invalidParameter
				elif int(self.Month) == 9:
					if self.usrDay > 30:
						raise invalidParameter
				elif int(self.Month) == 4:
					if self.usrDay > 30:
						raise invalidParameter
				elif int(self.Month) == 6:
					if self.usrDay > 30:
						raise invalidParameter
				elif int(self.Month) == 11:
					if self.usrDay > 30:
						raise invalidParameter
			else:
				raise requiredParameterMissing
				
			if self.Hour.strip():
				self.usrHour = int(self.Hour)
			else:
				raise requiredParameterMissing
				
			if self.Min.strip():
				self.usrMin = int(self.Min)
			else:
				self.usrMin = 0					# Allow null input of minutes
				#raise requiredParameterMissing
				
			if self.Sec.strip():
				self.usrSec = int(self.Sec)
			else:
				self.usrSec = 0					# Allow null input of seconds
				#raise requiredParameterMissing
			#
			#
			self.dataValidated = True
		except requiredParameterMissing:
			#print("!Aborted! - User failed to provide required information.")
			tk.messagebox.showerror(title="Action Aborted", message="User failed to provide required information.")
			self.dataValidated = False
		
		except invalidParameter:
			#print("!Aborted! - Information provided was unusable.")
			tk.messagebox.showerror(title="Action Aborted", message="Information provided was unusable.")
			self.dataValidated = False
		return 0
	
	
	def apply(self):
		if self.dataValidated:
			# Create a niave datetime object and pass tz and dst info
			niave_dt = datetime(self.usrYear, self.usrMonth, self.usrDay, self.usrHour, self.usrMin, self.usrSec)
			#Apply returns a new context object
			# (name, dt, tz_offset=None, applyDST=False, latitude=None, longitude=None, elevation=None)
			self.newContext=context.context(name=self.usrName, dt=niave_dt, tz_offset=self.usrTZ, applyDST=self.timeIsDST, latitude=self.usrLat, longitude=self.usrLon, elevation=self.usrElev)
			#
		return 0
	
		'''
		http://www.tcl.tk/man/tcl8.5/TkCmd/entry.htm#M-validate
		validate = 'key' or 'focusout', validatecommand = vcmd
		'''
		
	def testEntry(self, action, index, value_if_allowed, prior_value, keystroke, validation_type, trigger_type, widget_name):
		widget_name = widget_name.split('.')
		widget_name = widget_name[-1]
		# Entry widget tests are to allow or disallow keys presses
		# validate the actual values elsewhere, e.g. Feb has only 28/29 days.
		#
		# Remember these are ALL strings! Cast when needed!
		# Validation TURNS OFF if the tests return anything other then
		# True/False -- Including ERRORS
		if(action=='1'):
			if widget_name == 'inputMonth':
				if keystroke.isdigit() and 1 <= int(value_if_allowed) <= 12: return True
				else: return False

			elif widget_name == 'inputDay':
					if keystroke.isdigit() and 1 <= int(value_if_allowed) <= 31: return True
					else: return False
			
			# Swiss Ephemeris files sepl*.se1 and semo*.se1 were created from DE431, If ALL are downloaded,
			# covering the time range from 11 Aug. -12999 Jul. (= 4 May -12999 Greg.) to 7 Jan. 16800
			# Revisit!! - https://docs.python.org/3/library/time.html#time.struct_time
			# Python datetime only sypports 1 AD to 9999 AD, Investigate use of time.struct_time as a replacement.
			#
			elif widget_name == 'inputYear':
				if len(prior_value) == 0 and (keystroke == "-" or keystroke.isdigit()): return True
				#elif keystroke.isdigit() and -12998 < int(value_if_allowed) < 16799: return True
				elif keystroke.isdigit() and 1 < int(value_if_allowed) < 9999: return True
				else: return False

			elif widget_name == 'inputHour':
				if keystroke.isdigit() and 0 <= int(value_if_allowed) < 24: return True
				else: return False
			
			elif widget_name == 'inputMin' or widget_name == 'inputSec':
				if keystroke.isdigit() and (0 <= int(value_if_allowed) < 60 and len(value_if_allowed) <= 2): return True
				else: return False
			
			elif widget_name == 'inputTz':
				#Can't limit chr count after decimal - ROUND in the string validation
				if prior_value.count(".") == 0 and ((keystroke == "." and prior_value != "-") and len(prior_value) > 0): return True
				elif len(prior_value) == 0 and (keystroke == "-" or keystroke.isdigit()): return True
				elif (int(index) < 2	and prior_value.count(".") == 1) and not prior_value.startswith("-"): return True
				elif (int(index) < 3	and prior_value.count(".") == 1) and prior_value.startswith("-"): return True
				elif keystroke.isdigit() and -12 < float(value_if_allowed) < 12: return True
				else: return False
			
			elif widget_name == 'inputLocLat':
				if prior_value.count(".") == 0 and ((keystroke == "." and prior_value != "-") and len(prior_value) > 0): return True
				elif len(prior_value) == 0 and (keystroke == "-" or keystroke.isdigit()): return True
				elif prior_value == "-" and (keystroke.isdigit() and int(value_if_allowed) < 0): return True
				elif keystroke.isdigit() and -90 < float(value_if_allowed) < 90: return True
				else: return False
			
			elif widget_name == 'inputAltLatDeg': # +/- 90 (89.9)
				if keystroke.isdigit() and (len(value_if_allowed) <= 2 and 0 <= int(value_if_allowed) < 90): return True
				else: return False
			
			elif widget_name == 'inputAltLatNS':
				if len(value_if_allowed) == 1 and (keystroke.upper() == "N" or keystroke.upper() == "S"): return True
				else: return False
			
			elif widget_name == 'inputLocLon':
				if prior_value.count(".") == 0 and ((keystroke == "." and prior_value != "-") and len(prior_value) > 0): return True
				elif len(prior_value) == 0 and (keystroke == "-" or keystroke.isdigit()): return True
				elif prior_value == "-" and (keystroke.isdigit() and int(value_if_allowed) < 0): return True
				elif keystroke.isdigit() and -180 < float(value_if_allowed) < 180: return True
				else: return False
			
			elif widget_name == 'inputAltLonDeg': # +/- 180 (179.9)
				if keystroke.isdigit() and (len(value_if_allowed) <= 3 and 0 <= int(value_if_allowed) < 180): return True
				else: return False
			
			# Allow 2 digit for ARCMIN
			elif widget_name == 'inputAltLatMin' or widget_name == 'inputAltLonMin':
				if keystroke.isdigit() and (len(value_if_allowed) <= 2 and 0 <= int(value_if_allowed) < 60): return True
				else: return False
			
			# Allow 2 digit "." 1 digit for ARCSEC
			elif widget_name == 'inputAltLatSec' or widget_name == 'inputAltLonSec':
				if prior_value.count(".") == 0 and (keystroke == "." and len(prior_value) > 0): return True
				elif keystroke.isdigit() and (len(value_if_allowed) <= 4 and 0 <= float(value_if_allowed) < 60): return True
				else: return False
		
			elif widget_name == 'inputAltLonEW':
				if len(value_if_allowed) == 1 and (keystroke.upper() == "E" or keystroke.upper() == "W"): return True
				else: return False
			
			#Accept in Meters for Dead Sea, Jordan/Israel to Mount Everest's peak
			elif widget_name == 'inputLocElev':
				if len(prior_value) == 0 and (keystroke == "-" or keystroke.isdigit()): return True
				elif keystroke.isdigit() and -431 <= int(value_if_allowed) < 8848: return True
				else: return False
			
			else:
				return False
		else:	# Action was NOT INSERT. Allow it.
			return True



class Error(Exception):
	"""Base class for other exceptions"""
	pass

class requiredParameterMissing(Error):
	"""Raised when a required value is not provided"""
	pass

class invalidParameter(Error):
	"""Raised when the provided value is not part of this reality"""
	pass

