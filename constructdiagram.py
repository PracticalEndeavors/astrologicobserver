# Copyright (C) 2020  PracticalEndeavors.com, Alexander A. Genna
# GNU General Public License, version 2, see observer.py for license information.
import math
# Conver Polar Coordinates (r,θ) to Cartesian Coordinates (x,y):
# x = r * cos( θ )
# y = r * sin( θ )
#
# Note - math.sin/cos method expects arguments in radians!

class drawdiagram:
	def __init__(self, dataSet, canvas):
		'''	Processes a list of placement objects and diagram planet/sun position in daily rotation '''
		''' properties:																				'''
		'''																							'''
		''' methods:																				'''
		'''																							'''
		debug = 0	# 0 - False | 1 - True
		#
		self.dataSet = dataSet
		self.canvas = canvas
		
		reference_sequenceID  = len(dataSet) // 2
		self.reference_placement = dataSet[reference_sequenceID]
		
		
		# Configure dimensions based on canvas size
		canvas_w, canvas_h = int(self.canvas['width']), int(self.canvas['height'])
		
		halfWidth = canvas_w / 2
		halfHeight = canvas_h / 2
		cen_pt = (halfWidth,halfHeight)
		cen_x = cen_pt[0]
		cen_y = cen_pt[1]
		
		radius = halfWidth / 2
		tick = radius / 10
			
		#draw the horizon circle
		main = circlePoint(cen_pt, radius)
		self.canvas.create_oval(main[0][0], main[0][1], main[1][0], main[1][1], outline='black', width=1)
		self.canvas.create_line(((cen_x - radius) - tick),cen_y ,((cen_x + radius) + tick), cen_y, fill='black', width=2)
		self.canvas.create_text(((cen_x - radius) - tick -2),cen_y, text="ASC", anchor='e')
		self.canvas.create_text(((cen_x + radius) + tick +2),cen_y, text="DSC", anchor='w')
		
		# Get positions from placement
		reference_planetName = self.reference_placement.planetName
		pln = self.reference_placement.eclon
		sun = self.reference_placement.degPlacementSUN
		asc = self.reference_placement.asc
		mc = self.reference_placement.mc
		
		if debug: print("pln:", pln, "sun:", sun, "asc:", asc, "mc:", mc)
		
		# Rotate relative asc position
		if asc > 180:
			rotateEast = 180 + (360 - asc)
		else:
			rotateEast = 180 - asc
		
		r_pln = pln + rotateEast
		r_sun = sun + rotateEast
		r_mc = mc + rotateEast
		#
		# Get XY plot points
		r_pln_x = halfWidth + radius * math.cos(math.radians(r_pln))
		pt_pln_y = radius * math.sin(math.radians(r_pln))
		if pt_pln_y < 0: #Below the X Axis
			r_pln_y = halfHeight + math.fabs(pt_pln_y)
		else:
			r_pln_y = halfHeight - pt_pln_y
		
		r_sun_x = halfWidth + radius * math.cos(math.radians(r_sun))
		pt_sun_y = radius * math.sin(math.radians(r_sun))
		if pt_sun_y < 0: #Below the X Axis
			r_sun_y = halfHeight + math.fabs(pt_sun_y)
		else:
			r_sun_y = halfHeight - pt_sun_y
		
		# MC can't be below the X Axis
		r_mc_x = (halfHeight + tick) + radius * math.cos(math.radians(r_mc))
		r_mc_y = (halfHeight - tick) - radius * math.sin(math.radians(r_mc))
		
		# Set the size of the circle
		pln_pts = circlePoint((r_pln_x, r_pln_y), 3)
		sun_pts = circlePoint((r_sun_x, r_sun_y), 5)
		
		self.canvas.create_oval(sun_pts[0][0], sun_pts[0][1], sun_pts[1][0], sun_pts[1][1], fill='orange', outline='gold', width=2)
		# Don't draw the SUN on top of itself; Draw SUN first, so we can see cazimi
		if reference_planetName.upper() != "SUN":
			self.canvas.create_oval(pln_pts[0][0], pln_pts[0][1], pln_pts[1][0], pln_pts[1][1], fill='black', width=2)
		self.canvas.create_line(r_mc_x,r_mc_y,cen_x,cen_y, fill='red', width=1)
		self.canvas.create_text(r_mc_x, r_mc_y, text="MC", fill='red', anchor='s')
	# End of Class
		
def circlePoint(cen_pt, r):
	#return ULeft, LRight points of the oval
	uX = cen_pt[0] - r
	uY = cen_pt[1] + r
	lX = cen_pt[0] + r
	lY = cen_pt[1] - r
	return ((uX, uY), (lX,lY))
