# This application was programmed in python3 using tkinter UI toolkit.
#
# Astrologic Observer attempts to reignite the awareness for some of what has been lost to modern light
# pollution, or though the disconnection of contemporary astrological practice from direct observations
# of the "wandering stars" in the night sky. The planetary motion the ancients knew was much more than
# the longitudinal motion of the planets through the zodiac which is so singularly focused on today.
#
# With direct observation challenged by conditions, or when our focus is relegated to static charts; We've
# lost the ability to discern the relative contextual clues of bright vs. dim and fast vs. slow; Believing that
# to perceive these conditions again provides access to richer synthesis and interpretation, this software
# hopes to provide visual access to these attributes in a way that reference tables just do not do justice to.
#
# Changelog:
# 07-00-2020, development...
# 08-13-2020, version 1.00 released
#
# License Information:
# GNU General Public License, version 2
'''
	Copyright (C) 2020  PracticalEndeavors.com, Alexander A. Genna

	Astrologic Observer is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation version 2 of the
	license.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
'''
#
# Acknowledgements:
#	Thank you to all those whose work knowingly or otherwise assisted in the realization of this application;
#	In particular:
#	Hades Pluto, robert.pluto(at)gmail.com; Robert, Thank you - your work on "Valens" was both my first introduction
#		to and an instrumental reference to developing python/tkinter applications
#	Stanislas Marquis, stan(at)astrorigin.com; Stan, Thank you for your efforts with your project pyswisseph. https://github.com/astrorigin/pyswisseph.
#		your work was integral to this project coming to fruition.
#	Above all, Thank you to the AstroDienst Swiss Ephemeris library team. Without which none of the calculations presented here would be possible.
#		Dr. Alois Treindl, Dieter Koch, and community you've built; It is quite an amazing thing that you've made. https://www.astro.com/swisseph/
#	Finally, Thank you Kenneth Hirst, and Cosmorama Enterprises for the public release of the Astro.ttf Font which this application uses. https://sites.google.com/site/cosmoramaent/home/download
#
#
import tkinter as tk
import applicationframe as app
import se_exec

# Banner
applicationDisplayName = se_exec.applicationName + "\t" + se_exec.applicationVersion + "  \u00A92020"
print("\n" + applicationDisplayName + "\tGNU GPLv2 software")
se_exec.printSweVer()
#
appWindow=tk.Tk()
appWindow.geometry("%dx%d+25-25" % (appWindow.winfo_screenwidth() -50, appWindow.winfo_screenheight()//2))
observer=app.observer(appWindow)
observer.pack(expand=True, fill="both")

appWindow.mainloop()

