# Copyright (C) 2020  PracticalEndeavors.com, Alexander A. Genna
# GNU General Public License, version 2, see observer.py for license information.
# Default Location data for initialization
# Users should save/load stored unique events or set data in UI.
Lat 	= 40.884750
Lon 	= -74.008305
Elev	= 24						# Meters above sea-level

# User defined data directory, if not set the system data folder is used.
DataDir = ""				# Default location is "/data/"

# Operating Options
# Terms/Bounds default selection
BOUNDS = 0							# 0 - Egyptian | 1 - Ptolemy's
#
# Relationship to the SUN
ubsDeg = 15							# Under the Beams distance in deg
combustDeg = 8						# Combust distance in deg
cazimiDeg = 17 / 60 				# Cazimi distance in deg, 17 arc minutes
#
# Display Day/Night Sect visually with graph background color
dynamicBG = 1						# 0 - False | 1 - True
dlgDayChart = "lemon chiffon"		#"white smoke"
dlgNightChart = "midnight blue"		#"gray13"
#
referenceMark_color = "deep pink"
ingressLabels_color = "deep sky blue"
tropicsLatitudes_color = "deep sky blue"
celestialEquator_color = "dim gray"
ecliptic_color = "dim gray"
#intermediateLatitudes_color = "slate gray"
#
# Default graph theme colors. See /res/TKinter_Colors.py for full list of named colors
# "color", linewidth (in pixels)
#
# Magnitude Theme
invisible_dim_color,invisible_dim_weight = 				"goldenrod", 1
invisible_bright_color, invisible_bright_weight = 		"gold", 3
visible_dim_color, visible_dim_weight =					"gold", 1
visible_bright_color, visible_bright_weight =			"orange", 3
# Speed Theme
invisible_slow_color, invisible_slow_weight =			"slate gray", 1
invisible_fast_color, invisible_fast_weight =			"dark olive green", 3
invisible_very_fast_color, invisible_very_fast_weight =	"olive drab", 5
visible_slow_color, visible_slow_weight =				"dark green", 1
visible_fast_color, visible_fast_weight =				"forest green", 3
visible_very_fast_color, visible_very_fast_weight =		"lime green", 5
#
#
# Probably no one needs to touch these....
# Default Dialog Styling
# dlgRGB = (236, 236, 236)
dlgColor = '#{:02x}{:02x}{:02x}'.format(*(236, 236, 236))
#
# Default Graph Scaling
x_scaleMultiplier = 11.618
y_scaleMultiplier = 1
