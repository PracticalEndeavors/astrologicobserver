# Copyright (C) 2020  PracticalEndeavors.com, Alexander A. Genna
# GNU General Public License, version 2, see observer.py for license information.
# indexed lists
# 0 Aries, 1 Taurus...
zodiac = ["ARIES", "TAURUS", "GEMINI", "CANCER", "LEO", "VIRGO", "LIBRA", "SCORPIO", "SAGITTARIUS", "CAPRICORN", "AQUARIUS", "PISCES"]
# Map to ASTRO font...
astroZodiacFont = ["A","B","C","D","E","F","G","H","I","J","K","L"]

# 0 DAY, 1 NIGHT...
chartSect = ["DAY", "NIGHT"]
# 0 FIRE, 1 EARTH...
elements = ["FIRE", "EARTH", "AIR", "WATER"]
# etc...
modes = ["CARDINAL", "FIXED", "MUTABLE"]

# range lists
# Sign degree's by sign name
ARIES, TAURUS, GEMINI, CANCER, LEO, VIRGO, LIBRA, SCORPIO, SAGITTARIUS, CAPRICORN, AQUARIUS, PISCES = range(0, 360, 30)
# Sect
DAY, NIGHT = range(0, 2)
# Element
#FIRE, EARTH, AIR, WATER = range(0, 4)
# Modality
#CARDINAL, FIXED, MUTABLE = range(0, 3)
#
# Essential dignities
# -1 returned for no planetary assignment
sign_rul = (4, 3, 2, 1, 0, 2, 3, 4, 5, 6, 6, 5)
sign_det = (3, 4, 5, 6, 6, 5, 4, 3, 2, 1, 0, 2)
sign_ex  = (0, 1,-1, 5,-1, 2, 6,-1,-1, 4,-1, 3)
sign_fa  = (6,-1,-1, 4,-1, 3, 0, 1,-1, 5,-1, 2)
#
# Triplicites
trip_fire_day = (0, 5, 6) #Sun, Jupiter, Saturn
trip_earth_day = (3, 1, 4) #Venus, Moon, Mars
trip_air_day = (6, 2, 5) #Saturn, Mercury, Jupiter
trip_water_day = (3, 4, 1) #Venus, Mars, Moon
#
trip_fire_night = (5, 0, 6) #Jupiter, Sun, Saturn
trip_earth_night = (1, 3, 4) #Moon, Venus, Mars
trip_air_night = (2, 6, 5) #Mercury, Saturn, Jupiter
trip_water_night = (4, 3, 1) #Mars, Venus, Moon

#
# Bounds list pairs (start degree, lord) by Sign index - 0 Aries, 1 Taurus...
# BoundSet determined by BOUNDS value - > 0 Egyptian, < 0 Ptolemy (Ptolemy requires sect)
# returns SE-Planet ID
bounds_egyptian = [
((0, 5),(6, 3),(12, 2),(20, 4),(25, 6)),
((0, 3),(8, 2),(14, 5),(22, 6),(27, 4)),
((0, 2),(6, 5),(12, 3),(17, 4),(24, 6)),
((0, 4),(7, 3),(13, 2),(19, 5),(26, 6)),
((0, 5),(6, 3),(11, 6),(18, 2),(24, 4)),
((0, 2),(7, 3),(17, 5),(21, 4),(28, 6)),
((0, 6),(6, 2),(14, 5),(21, 3),(28, 4)),
((0, 4),(7, 3),(11, 2),(19, 5),(24, 6)),
((0, 5),(12, 3),(17, 2),(21, 6),(26, 4)),
((0, 2),(7, 5),(14, 3),(22, 6),(26, 4)),
((0, 2),(7, 3),(13, 5),(20, 4),(25, 6)),
((0, 3),(12, 5),(16, 2),(19, 4),(28, 6))]

bounds_ptolemy_day = [
((0, 5),(6, 3),(14, 2),(21, 4),(26, 6)),
((0, 3),(8, 2),(15, 5),(22, 6),(24, 4)),
((0, 2),(7, 5),(13, 3),(20, 4),(26, 6)),
((0, 4),(6, 2),(13, 5),(20, 3),(27, 6)),
((0, 5),(6, 2),(13, 6),(19, 5),(25, 4)),
((0, 2),(7, 3),(13, 5),(18, 6),(24, 4)),
((0, 6),(6, 3),(11, 2),(19, 5),(24, 4)),
((0, 4),(6, 3),(14, 5),(21, 2),(27, 6)),
((0, 5),(8, 3),(14, 2),(19, 6),(25, 4)),
((0, 3),(6, 2),(12, 5),(19, 6),(25, 4)),
((0, 6),(6, 2),(12, 3),(20, 5),(25, 4)),
((0, 3),(8, 5),(14, 2),(20, 4),(27, 5))]

bounds_ptolemy_night = [
((0, 5),(6, 3),(14, 2),(21, 4),(26, 6)),
((0, 3),(8, 2),(15, 5),(22, 6),(24, 4)),
((0, 2),(7, 5),(13, 3),(20, 4),(26, 6)),
((0, 4),(6, 5),(13, 2),(20, 3),(27, 6)),
((0, 6),(6, 2),(13, 3),(19, 5),(25, 4)),
((0, 2),(7, 3),(13, 5),(18, 6),(24, 4)),
((0, 6),(6, 3),(11, 5),(19, 2),(24, 4)),
((0, 4),(6, 5),(14, 3),(21, 2),(27, 6)),
((0, 5),(8, 3),(14, 2),(19, 6),(25, 4)),
((0, 3),(6, 2),(12, 5),(19, 4),(25, 6)),
((0, 6),(6, 2),(12, 3),(20, 5),(25, 4)),
((0, 3),(8, 5),(14, 2),(20, 4),(27, 5))]

#
# Decan list each 10 degrees of Sign index - 0 Aries, 1 Taurus...
# returns SE-Planet ID
decan = [
(4, 0, 3),
(2, 1, 6),
(5, 4, 0),
(3, 2, 1),
(6, 5, 4),
(0, 3, 2),
(1, 6, 5),
(4, 0, 3),
(2, 1, 6),
(5, 4, 0),
(3, 2, 1),
(6, 5, 4)]
