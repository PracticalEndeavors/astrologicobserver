To launch this software, depending on your OS, you may simply execute the
file OBSERVER.py. For other systems you may need to call PYTHON3 to execute the
same file with commands similar to the following;
	
	python3 observer.py &
