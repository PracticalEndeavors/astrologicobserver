# Copyright (C) 2020  PracticalEndeavors.com, Alexander A. Genna
# GNU General Public License, version 2, see observer.py for license information.
import se_exec
import defaults
import math
import time
from datetime import timedelta, datetime, tzinfo, timezone

class context:
	def __init__(self, name, dt, tz_offset=None, applyDST=False, latitude=None, longitude=None, elevation=None):
		''' Facts provided about the event date/time/location 						'''
		''' provides: 																'''
		''' name, latitude, longitude, elevation, dgeo, jdate, 				 		'''
		''' awareDT, displayDateStr 												'''
		self.name = name
		self.applyDST = applyDST
		self.tz_offset = tz_offset
		
		# LAT/LON in dec deg, elevation is double?
		if latitude == None: self.latitude = defaults.Lat
		else: self.latitude = latitude

		if longitude == None: self.longitude = defaults.Lon
		else: self.longitude = longitude
		
		if elevation == None: self.elevation = defaults.Elev		# Elevation required for swe_vis_limit_mag - DGEO arg
		else: self.elevation = elevation

		# Watch the input order LON first - LAT next.
		self.dgeo = (self.longitude, self.latitude, self.elevation)	# DGEO - decdeg-LON, decdeg-LAT, altitude (eye height) in meters.
		#
		#test if datetime is aware or not
		if dt.tzinfo == None:
			niaveDT = dt											#Assume this came from user input
			
			# If niave, and DST change the provided tz_offset before use
			if self.applyDST:
				self.tz_offset   = self.tz_offset   + 1
			
			# Create a tzinfo object from the offset
			if self.tz_offset   < 0:
				sign = -1
			else:
				sign = 1
			tzminutes = int((math.fabs(self.tz_offset  ) % 1) * 60)
			tzhours = int(math.fabs(self.tz_offset ))
			tzinfo = timezone(sign * timedelta(hours=tzhours, minutes=tzminutes))
			
			self.awareDT = datetime(niaveDT.year, niaveDT.month, niaveDT.day, niaveDT.hour, niaveDT.minute, niaveDT.second).replace(tzinfo = tzinfo)
		
		else:
			self.awareDT = dt											#Assume this came from __init__ - call as dt=self.datetime.now().astimezone()
			awareTimedelta = self.awareDT.utcoffset()					#swe needs the TZ_Offset as a number
			offset = (awareTimedelta.days, awareTimedelta.seconds)
			# e.g. returns (-1,72000)
			tzdays_hr = offset[0]*24
			tzseconds_hr = offset[1]/3600
			self.tz_offset   = tzdays_hr + tzseconds_hr
		#
		#
		# Convert to UTC for SWE
		sweUTC = se_exec.swe.utc_time_zone(self.awareDT.year, self.awareDT.month, self.awareDT.day, self.awareDT.hour, self.awareDT.minute, self.awareDT.second, self.tz_offset  )
		# WTF!! - sweUTC[5] keeps returning -e negative exponential seconds
		# Substituting datetime objects seconds seems innocuous for our use case
		self.jdate = se_exec.swe.utc_to_jd(sweUTC[0], sweUTC[1], sweUTC[2], sweUTC[3], sweUTC[4], self.awareDT.second, se_exec.swe.GREG_CAL)[1] #self.jdate = se_exec.swe.utc_to_jd(sweUTC[0], sweUTC[1], sweUTC[2], sweUTC[3], sweUTC[4], int(sweUTC[5]), se_exec.swe.GREG_CAL)[1]
		#
		formattedLocalDateTime = self.awareDT.strftime("%a %b %d %Y %I:%M:%S %p %Z") # + "  [ " + str(self.jdate) + " ]" #jDate is a bit much here
		self.displayDateStr = formattedLocalDateTime

	#def localTz_offset(self):
	# Get timezone from the system OS.
	#	if time.daylight:
	#		offsetHour = time.altzone / 3600
	#	else:
	#		offsetHour = time.timezone / 3600
	#	return offsetHour
