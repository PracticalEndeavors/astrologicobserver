# Copyright (C) 2020  PracticalEndeavors.com, Alexander A. Genna
# GNU General Public License, version 2, see observer.py for license information.
# indexed lists SWE Planet values
# 0 SUN, 1 MOON...
classicBodies	= ["SUN","MOON","MERCURY","VENUS","MARS","JUPITER","SATURN"]
#modernBodies	= ["SUN","MOON","MERCURY","VENUS","MARS","JUPITER","SATURN","URANUS","NEPTUNE","PLUTO"]
# Map to ASTRO font... Last key is Retrograde "M"
astroZodiacFont = ["Q","R","S","T","U","V","W","X","Y","Z","M"]
astroNodeFont = ["<",">"] # NNode, SNode

# range lists
# Body by planet name
SUN, MOON, MERCURY, VENUS, MARS, JUPITER, SATURN, URANUS, NEPTUNE, PLUTO = range(0, 10)

tropic_of_cancer=23.43		# +23°26'
tropic_of_capricorn=-23.43	# -23°26'

#
# Sidereal orbital periods in days
# Earth @ 365.256363 days, used for the SUN due to apparent motion referenced from earth.
# Ordered by SWE planetary list - SUN, MOON, MERCURY...
orbital_periods = ((0,365.256),(1,27.322),(2,87.969),(3,224.701),(4,693.987),(5,4346.550),(6,10775.062),(7,30681.534),(8,60194.248),(9,90766.206))
# Synodic periods in days
# Ordered by SWE planetary list
synodic_periods = ((0,-1),(1,29.531),(2,115.88),(3,583.92),(4,779.96),(5,398.88),(6,378.09),(7,369.66),(8,367.49),(9,366.73))
#
# Average daily motion - decimal degree per day: FAST if greater, SLOW if less than
daily_motion = ((0,0.985716119),(1,13.14429121),(2,0.967897976),(3,0.986234748),(4,0.545140993),(5,0.082211918),(6,0.082307958))
#
# VERY FAST daily motion  - Fastest daily motion less 10% of difference from average direct motion.
fast_motion = ((0,1.016313579),(1,14.1688077),(2,1.802218923),(3,1.217502745),(4,0.680105261),(5,0.226067802),(6,0.226026285))
#
# Apparent Magnitude scale is "backwards" and logarithmic. Larger magnitudes correspond to fainter stars.
# m2-m1 = -2.50 log(B2/B1) - Max,Min values from wikipedia
apparent_magnitude = ((-26.74,-26.74),(-12.90,-2.5),(-2.48,7.25),(-4.92,-2.98),(-2.94,1.86),(-2.94,-1.66),(-0.55,1.17))
