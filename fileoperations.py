# Copyright (C) 2020  PracticalEndeavors.com, Alexander A. Genna
# GNU General Public License, version 2, see observer.py for license information.
import tkinter as tk
import tkinter.ttk as ttk
from tkinter import messagebox
import os
import pickle
import defaults
''' Save to and Load from file functions 										'''
''' Saves the CONTEXT object and replaces currently loaded with data from file. '''

class filesys():
	def __init__(self):
		# Identify storage location.
		# Use the user defined location unless one not provided
		CWD = os.getcwd()
		if not defaults.DataDir:
			saveToFolder = CWD + "/data/"
		else:
			saveToFolder = defaults.DataDir.replace("\\", "/")
			if saveToFolder[-1] != "/":
				saveToFolder = saveToFolder + "/"
		# Make sure this location exists..
		if not os.path.exists(saveToFolder):
			tk.messagebox.showerror(title="Location Validation", message="User Defaults Set an Invalid Folder Path.\nUsing default '/data/' directory.")
			# resolves macos issue with messagebox lingering after close
			tk._default_root.grab_set()
			tk._default_root.grab_release()
			#
			saveToFolder = CWD + "/data/"
			
		self.saveToFolder = saveToFolder
		#print(self.saveToFolder)
		
	def saveLocation(self):
		return self.saveToFolder
		
	def saveToFile(self, object):
		# Filename from name of context
		filename = object.name.strip()
		filename = filename.replace(" ", "_") + ".context"
		absFN = self.saveToFolder + filename
		#
		# Try block for the write.
		try:
			fHandle = open(absFN, 'wb')
			pickle.dump(object, fHandle)
			fHandle.close()
			return True
		except IOError:
			tk.messagebox.showerror(message="Error Saving File.")
			# resolves macos issue with messagebox lingering after close
			tk._default_root.grab_set()
			tk._default_root.grab_release()
			return False

	def loadFromFile(self, filename):
		# Return Context object from file
		absFN = self.saveToFolder + filename
		try:
			fHandle = open(absFN, 'rb')
			context = pickle.load(fHandle)
			fHandle.close()
			return context
		except EOFError:
			print("Error Loading File.")
			return None
