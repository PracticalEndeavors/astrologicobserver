# Copyright (C) 2020  PracticalEndeavors.com, Alexander A. Genna
# GNU General Public License, version 2, see observer.py for license information.
import math
''' functions.py Provides:								'''
'''	returnDecimalDegree, returnDMSLat, returnDMSLon		'''
'''														'''

#
# Convert DMS values to decimal degree values
def returnDecimalDegree(degree, hemisphere, arcminute, arcsecond):
	#
	if hemisphere.upper() == "S" or hemisphere.upper() == "W": sign = False
	else: sign = True
	#
	asec = float(arcsecond) / 3600
	amin = int(arcminute) / 60
	decimalDegree = int(degree) + amin + asec
	if not sign:
		return -decimalDegree
	else:
		return decimalDegree
		
# Convert decimal degree values to DMS values
# Call different functions for Lat / Lon, code is more understandable
def returnDMSLat(degreeValue):
	#
	val = math.fabs(float(degreeValue))
	deg = val // 1
	minutes = round(60 * (val - deg),6) 	# round here fixes rounding drift for some values loss of 1/10 arcsec @ xxD 22' 0.0"
	asec = 60 * (minutes - int(minutes))	# keep full arcsec return * vales truncates in the SET() call to fill the dialog box on the editContext()
	deg = int(deg)
	amin = int(minutes)
	if degreeValue > 0:
		return (deg, "N", amin, asec)
	else:				#Is negative
		return (deg, "S", amin, asec)

def returnDMSLon(degreeValue):
	#
	val = math.fabs(float(degreeValue))
	deg = val // 1
	minutes = round(60 * (val - deg),6)		# round here fixes rounding drift for some values loss of 1/10 arcsec @ xxD 22' 0.0"
	asec = 60 * (minutes - int(minutes))	# keep full arcsec return * vales truncates in the SET() call to fill the dialog box on the editContext()
	deg = int(deg)
	amin = int(minutes)
	if degreeValue > 0:
		return (deg, "E", amin, asec)
	else:				#Is negative
		return (deg, "W", amin, asec)

def round_up(n, decimals=0):
	multiplier = 10 ** decimals
	return math.ceil(n * multiplier) / multiplier

def round_down(n, decimals=0):
	multiplier = 10 ** decimals
	return math.floor(n * multiplier) / multiplier

def truncate(n, decimals=0):
	multiplier = 10 ** decimals
	return int(n * multiplier) / multiplier
